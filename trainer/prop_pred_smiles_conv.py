# Imports

import msgpack
import argparse
import keras
import nesvit.prediction.representation as rep
import nesvit.prediction.model as archs
import nesvit.util.checkpoints as checks
import nesvit.dataset.qm9 as qm9

from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau
from keras.callbacks import EarlyStopping
from tensorflow.python.lib.io import file_io


def init(_args):

	# Load data

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))

	# Get training set

	t_data, t_labels, unique_chars, max_smiles_len = rep.get_qm9_smiles_training_set(data, _args.label, 1)

	# Optimizer

	optimizer = keras.optimizers.Adam(lr = 0.001)

	# Model

	model = archs.build_model_1_1(input_shape = (max_smiles_len, len(unique_chars)), optimizer = optimizer, loss = 'mse', metrics = ['mae', 'mean_absolute_percentage_error'])
	model.summary()

	# Train model

	model_prefix = _args.model_prefix + '_' + _args.label
	file_name = model_prefix + '_{epoch:02d}_{mean_absolute_error:.4f}.h5'
	checkpoint = ModelCheckpoint(file_name, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
	move_checkpoint = checks.MoveCheckpoint(_args.output_dir, model_prefix)
	reduce_learning_rate = ReduceLROnPlateau(monitor = 'loss', factor = 0.25, patience = 3, min_lr = 0.0000001, mode = 'min', verbose = 1)
	early_stopping = EarlyStopping(monitor = 'val_loss', min_delta = 0, patience = 20, verbose = 1, mode = 'min')

	history = model.fit([t_data],
						t_labels,
						shuffle = False,
						epochs = int(_args.epochs),
						batch_size = int(_args.batch_size),
						validation_split = 0.2,
						verbose = 1,
						callbacks = [checkpoint, move_checkpoint, early_stopping])


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--job-dir',
						default = '',
						help = 'GCS job directory',
						required = True)

	parser.add_argument('--input-file',
						default = '../input/data_10.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--output-dir',
						default = '../output',
						help = 'GCS or local path to output directory',
						required = False)

	parser.add_argument('--label',
						default = 'gap',
						help = 'Model name prefix',
						required = False)

	parser.add_argument('--model-prefix',
						default = 'm_smiles',
						help = 'Model name prefix',
						required = False)

	parser.add_argument('--epochs',
						default = '50',
						required = False)

	parser.add_argument('--batch-size',
						default = '32',
						required = False)

	args = parser.parse_args()

	init(args)

