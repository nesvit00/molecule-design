# Imports

import argparse
import msgpack
import keras
import nesvit.prediction.representation as rep
import nesvit.prediction.model as archs
import nesvit.util.checkpoints as checks

from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau
from keras.callbacks import EarlyStopping
from tensorflow.python.lib.io import file_io


def init(_args):

	# Variables

	resolution = int(_args.resolution)

	# Load data

	print('Load data')

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))

	print('Data loaded... voxelize')

	t_data, t_labels = rep.get_qm9_voxelized_charge_training_set(data, _args.label, int(_args.resolution))

	# Optimizer

	optimizer = keras.optimizers.Adam(lr = 0.001)

	# Model

	input_shape = (resolution, resolution, resolution)
	model = archs.build_model_3(input_shape = input_shape, optimizer = optimizer, loss = 'mse', metrics = ['mae', 'mean_absolute_percentage_error'])
	model.summary()

	# Train model

	file_name = _args.model_prefix + '_' + \
				_args.label + '_' + \
				_args.resolution + '_' + \
				'{epoch:02d}_{mean_absolute_error:.4f}.h5'

	checkpoint = ModelCheckpoint(file_name, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
	move_checkpoint = checks.MoveCheckpoint(_args.output_dir, _args.model_prefix)
	reduce_learning_rate = ReduceLROnPlateau(monitor = 'loss', factor = 0.2, patience = 4, min_lr = 0.00005, mode = 'min', verbose = 1)
	early_stopping = EarlyStopping(monitor = 'val_loss', min_delta = 0, patience = 20, verbose = 1, mode = 'min')

	history = model.fit([t_data],
						t_labels,
						epochs = int(_args.epochs),
						batch_size = int(_args.batch_size),
						validation_split = 0.2,
						verbose = 1,
						callbacks = [checkpoint, move_checkpoint, reduce_learning_rate])


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--job-dir',
						default = '',
						help = 'GCS job directory',
						required = True)

	parser.add_argument('--input-file',
						default = '../input/data_10.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--output-dir',
						default = '../output/',
						help = 'GCS or local path to output directory',
						required = False)

	parser.add_argument('--label',
						default = 'gap',
						required = False)

	parser.add_argument('--model-prefix',
						default = 'm_voxel_charge',
						help = 'Model name prefix',
						required = False)

	parser.add_argument('--epochs',
						default = '100',
						help = 'Number of training epochs',
						required = False)

	parser.add_argument('--batch-size',
						default = '128',
						required = False)

	parser.add_argument('--resolution',
						default = '50',
						required = False)

	args = parser.parse_args()

	init(args)

