# Imports

import msgpack
import argparse
import keras
import nesvit.prediction.model as archs
import nesvit.util.checkpoints as checks
import nesvit.dataset.qm9 as qm9

from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau
from tensorflow.python.lib.io import file_io


def init(_args):

	# Load data

	t_data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))
	p_data = msgpack.unpack(file_io.FileIO(_args.input_prop_file, 'rb'))

	# Normalize

	# Get property targets by label

	p_data = qm9.get_property_targets(p_data, _args.label)

	# Model

	optimizer = keras.optimizers.Adam(lr = 0.001)

	model = archs.build_model_2(input_shape = (len(t_data[0]),), optimizer = optimizer, loss = 'mse', metrics = ['mae', 'mean_absolute_percentage_error'])
	model.summary()

	# Train model

	file_name = _args.model_prefix + '_' + _args.label + '_{epoch:02d}_{mean_absolute_error:.4f}.h5'
	checkpoint = ModelCheckpoint(file_name, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
	move_checkpoint = checks.MoveCheckpoint(_args.output_dir, _args.model_prefix)
	reduce_learning_rate = ReduceLROnPlateau(monitor = 'loss', factor = 0.2, patience = 4, min_lr = 0.00001, mode = 'min', verbose = 1)
	early_stopping = EarlyStopping(monitor = 'val_loss', min_delta = 0, patience = 20, verbose = 1, mode = 'min')

	history = model.fit([t_data],
						p_data,
						shuffle = True,
						epochs = int(_args.epochs),
						batch_size = int(_args.batch_size),
						validation_split = 0.2,
						verbose = 1,
						callbacks = [checkpoint, move_checkpoint, reduce_learning_rate, early_stopping])


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--job-dir',
						default = '',
						help = 'GCS job directory',
						required = True)

	parser.add_argument('--input-file',
						default = '../input/data_coulomb_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--input-prop-file',
						default = '../input/data_prop_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--output-dir',
						default = '../output/',
						help = 'GCS or local path to output directory',
						required = False)

	parser.add_argument('--label',
						default = 'gap',
						help = 'Model name prefix',
						required = False)

	parser.add_argument('--model-prefix',
						default = 'm_coulomb',
						help = 'Model name prefix',
						required = False)

	parser.add_argument('--epochs',
						default = '50',
						required = False)

	parser.add_argument('--batch-size',
						default = '128',
						required = False)

	args = parser.parse_args()

	init(args)

