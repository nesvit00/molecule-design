# Imports

import argparse
import msgpack
import keras
import nesvit.dataset.qm9 as qm9
import nesvit.prediction.representation as rep
import nesvit.prediction.model as archs
import nesvit.util.checkpoints as checks

from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau
from tensorflow.python.lib.io import file_io


def init(_args):

	# Variables

	resolution = int(_args.resolution)

	# Load data

	print('Load data')

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))

	print('Data loaded... voxelize')

	t_data, t_labels = rep.get_qm9_voxelized_training_set(data, _args.label, int(_args.resolution), int(_args.sigma), int(_args.omega))

	# Optimizer

	optimizer = keras.optimizers.Adam(lr = 0.001)

	# Model

	input_shape = (resolution, resolution, resolution, qm9.ATOM_N)
	model = archs.build_model_4(input_shape = input_shape, optimizer = optimizer, loss = 'mse', metrics = ['mae', 'mean_absolute_percentage_error'])
	model.summary()

	# Train model

	file_name = _args.model_prefix + '_' + \
				_args.label + '_' + \
				_args.resolution + '_' + \
				_args.sigma + '_' + \
				_args.omega + '_' + \
				'{epoch:02d}_{mean_absolute_error:.4f}.h5'

	checkpoint = ModelCheckpoint(file_name, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
	move_checkpoint = checks.MoveCheckpoint(_args.output_dir, _args.model_prefix)
	reduce_learning_rate = ReduceLROnPlateau(monitor = 'loss', factor = 0.2, patience = 2, min_lr = 0.000001,
											 mode = 'min', verbose = 1)

	history = model.fit([t_data],
						t_labels,
						epochs = int(_args.epochs),
						batch_size = int(_args.batch_size),
						validation_split = 0.2,
						verbose = 1,
						callbacks = [checkpoint, move_checkpoint, reduce_learning_rate])


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--job-dir',
						default = '',
						help = 'GCS job directory',
						required = True)

	parser.add_argument('--input-file',
						default = '../input/data_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--output-dir',
						default = '../output/',
						help = 'GCS or local path to output directory',
						required = False)

	parser.add_argument('--label',
						default = 'gap',
						required = False)

	parser.add_argument('--model-prefix',
						default = 'm_voxel',
						help = 'Model name prefix',
						required = False)

	parser.add_argument('--epochs',
						default = '10',
						help = 'Number of training epochs',
						required = False)

	parser.add_argument('--batch-size',
						default = '36',
						required = False)

	parser.add_argument('--resolution',
						default = '20',
						required = False)

	parser.add_argument('--sigma',
						default = '1',
						required = False)

	parser.add_argument('--omega',
						default = '0',
						required = False)

	args = parser.parse_args()

	init(args)

