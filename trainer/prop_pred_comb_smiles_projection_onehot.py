# Imports

import msgpack
import argparse
import keras
import numpy as np
import nesvit.prediction.representation as rep
import nesvit.prediction.model as archs
import nesvit.util.checkpoints as checks

from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau
from keras.callbacks import EarlyStopping
from tensorflow.python.lib.io import file_io


def init(_args):

	# Variables

	proj_n = 3
	resolution = int(_args.resolution)

	# Load data

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))

	# Get training set

	t_smiles_data, t_smiles_labels, unique_chars, max_smiles_len = rep.get_qm9_smiles_training_set(data, _args.label, 1)
	t_proj_data, t_proj_labels = rep.get_qm9_projected_stacked_onehot_training_set(data, _args.label, int(_args.resolution), proj_n)

	# Optimizer

	optimizer = keras.optimizers.Adam(lr = 0.001)

	# Model

	channels = len(t_proj_data[0][0][0])
	model = archs.build_model_comb_smiles_projection_onehot_1(input_shape_smiles = (max_smiles_len, len(unique_chars)),
												input_shape_proj = (resolution * proj_n, resolution, channels),
												optimizer = optimizer,
												loss = 'mse',
												metrics = ['mae', 'mean_absolute_percentage_error'])
	model.summary()

	# Train model

	file_name = _args.model_prefix + '_' + _args.label + '_{epoch:02d}_{mean_absolute_error:.4f}.h5'
	checkpoint = ModelCheckpoint(file_name, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
	move_checkpoint = checks.MoveCheckpoint(_args.output_dir, _args.model_prefix)
	reduce_learning_rate = ReduceLROnPlateau(monitor = 'loss', factor = 0.2, patience = 4, min_lr = 0.00005, mode = 'min', verbose = 1)
	early_stopping = EarlyStopping(monitor = 'val_loss', min_delta = 0, patience = 20, verbose = 1, mode = 'min')

	history = model.fit({'smiles_input': np.array(t_smiles_data), 'proj_input': np.array(t_proj_data)},
						{'main_output': np.array(t_smiles_labels)},
						epochs = int(_args.epochs),
						batch_size = int(_args.batch_size),
						validation_split = 0.2,
						verbose = 1,
						callbacks = [checkpoint, move_checkpoint])


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--job-dir',
						default = '',
						help = 'GCS job directory',
						required = True)

	parser.add_argument('--input-file',
						default = '../input/data_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--input-bob-file',
						default = '../input/data_bob_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--output-dir',
						default = '../output/',
						help = 'GCS or local path to output directory',
						required = False)

	parser.add_argument('--label',
						default = 'gap',
						help = 'Model name prefix',
						required = False)

	parser.add_argument('--model-prefix',
						default = 'm_smiles_proj_onehot',
						help = 'Model name prefix',
						required = False)

	parser.add_argument('--resolution',
						default = '50',
						required = False)

	parser.add_argument('--epochs',
						default = '50',
						required = False)

	parser.add_argument('--batch-size',
						default = '128',
						required = False)

	args = parser.parse_args()

	init(args)

