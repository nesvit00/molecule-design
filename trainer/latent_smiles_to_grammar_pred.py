# Imports

import argparse
import msgpack
import numpy as np
import nesvit.gvae.model as model
import nesvit.gvae.parser as gvae_parser
import nesvit.util.checkpoints as checks
import nesvit.prediction.representation as rep
import nesvit.dataset.qm9 as qm9

from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau
from tensorflow.python.lib.io import file_io


def init(_args):

	# Variables

	max_sample_len = int(_args.max_sample_len)
	latent_dim = int(_args.latent_dim)
	prod_rule_n = int(_args.prod_rule_n)

	# Load data

	smiles_data = msgpack.unpack(file_io.FileIO(_args.input_smiles_file, 'rb'))
	y_data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))
	x_data, p_target, unique_chars, max_smiles_len = rep.get_qm9_smiles_training_set(smiles_data, _args.label, 1)

	print('Data loaded')

	gparser = gvae_parser.GrammarParser(max_sample_len)
	gmodel = model.SMILES2GrammarRegressorModel(1, max_smiles_len, len(unique_chars), max_sample_len, prod_rule_n, latent_dim, gparser.rule_index, gparser.mask)
	gmodel.vae.summary()

	file_name = _args.model_prefix + '_{epoch:02d}' + '_slen_' + str(max_sample_len) + '_zdim_' + str(latent_dim) + '_loss_{loss:.4f}' + '_mae_{prop_pred_mean_absolute_error:.4f}' + '.h5'
	checkpoint = ModelCheckpoint(file_name, monitor = 'loss', verbose = 1, save_best_only = True, mode = 'min')
	move_checkpoint = checks.MoveCheckpoint(_args.output_dir, _args.model_prefix)
	reduce_learning_rate = ReduceLROnPlateau(monitor = 'loss', factor = 0.5, patience = 1, min_lr = 0.0000001, mode = 'auto', verbose = 1)

	history = gmodel.vae.fit({'encoder_input': np.array(x_data)},
							 {'y_out': np.array(y_data), 'prop_pred': np.array(p_target)},
							 shuffle = True,
							 epochs = int(_args.epochs),
							 batch_size = int(_args.batch_size),
							 validation_split = 0.2,
							 verbose = 1,
							 callbacks = [checkpoint, move_checkpoint, reduce_learning_rate])


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--job-dir',
						default = '',
						help = 'GCS job directory',
						required = True)

	parser.add_argument('--input-file',
						default = '../input/data_gvae_1000_126.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--input-smiles-file',
						default = '../input/data_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--output-dir',
						default = '../output/',
						help = 'GCS or local path to output directory',
						required = False)

	parser.add_argument('--model-prefix',
						default = 'm_smiles_to_grammar_pred',
						help = 'Model name prefix',
						required = False)

	parser.add_argument('--label',
						default = 'gap',
						help = 'Model name prefix',
						required = False)

	parser.add_argument('--epochs',
						default = 100,
						help = 'Number of training epochs',
						required = False)

	parser.add_argument('--batch-size',
						default = 128,
						help = 'Size of the batch',
						required = False)

	parser.add_argument('--max-sample-len',
						default = 126,
						required = False)

	parser.add_argument('--latent-dim',
						default = 36,
						required = False)

	parser.add_argument('--prod-rule-n',
						default = 54,
						required = False)

	args = parser.parse_args()

	init(args)

