# Imports

import os
import glob
import msgpack

# Constants

KCAL_PER_MOL_IN_Ha = 627.509474
EV_IN_Ha = 27.21138602
CM_IN_HA = 219474.6313702

QM9_PROPERTY_LABELS = ['A', 'B', 'C', 'mu', 'alpha', 'homo', 'lumo', 'gap', 'r2', 'zpve', 'U0', 'U', 'H', 'G', 'Cv']

QM9_PROPERTY_ALTERNATIVE_UNITS = [0.01, 1, 1, 1, 1, KCAL_PER_MOL_IN_Ha, KCAL_PER_MOL_IN_Ha, KCAL_PER_MOL_IN_Ha, 0.1, 0.001 * CM_IN_HA, 0.001 * KCAL_PER_MOL_IN_Ha, 0.001 * KCAL_PER_MOL_IN_Ha, 0.001 * KCAL_PER_MOL_IN_Ha, 0.001 * KCAL_PER_MOL_IN_Ha, 1]

# Computed on QM9 dataset: get_min_max_property_value
QM9_PROPERTY_MIN_MAX = [[0.0, 619867.68314], [0.33712, 437.90386], [0.33118, 282.94545], [0.0, 29.5564],
						[6.31, 196.62], [-0.4286, -0.1017], [-0.175, 0.1935], [0.0246, 0.6221],[19.0002, 3374.7532],
						[0.015951, 0.273944], [-675.265273, -40.47893], [-675.258505, -40.476062],
						[-675.257561, -40.475117], [-675.297105, -40.498597], [6.002, 46.969]]

QM9_PROPERTY_NOMR_IN_0_1_SPACE = [2.0026572593326275, 24.05582367304833, 26.765214619315586, 1191.5438800083912,
								  975.4892682885851, 1471.4519751198272, 3088.5622880686974, 1952.3661747057065,
								  1827.2813972167005, 3000.1781673518317, 1418.1992263643438, 1418.200624184775,
								  1418.2006220830733, 1418.189818596817, 2325.06639033043]

SMILES_UNIQUE_CHARS = [['#', '(', ')', '+', '-', '1', '2', '3', '4', '5', '=', 'C', 'F', 'H', 'N', 'O', '[', ']'],
					   ['#', '(', ')', '.', '1', '2', '3', '4', '5', '=', '@', 'C', 'F', 'H', 'N', 'O', '[', ']', 'c', 'n', 'o']]
SMILES_MAX_SAMPLE_LEN = [30, 60]
MAX_ATOM_POS = 14  # Computed on QM9 dataset
MAX_MOLECULE_SIZE = 29  # Max size of 29 atoms among full QM9 dataset
ATOM_LABELS = ['H', 'C', 'N', 'O', 'F']
ATOM_AMOUNT_MAX = [20, 9, 7, 5, 6]
ATOM_MASS = [1.007, 12.0, 14.003, 15.994, 18.998]
ATOM_ORDINAL_NUMBER = [1, 6, 7, 8, 9]
ATOM_N = len(ATOM_LABELS)
Q_PROJ = [0.03, 0.02, 0.01]


class QM9Item:
	"""QM9 Item

	Args:
		id (int): gdb id
	"""

	def __init__(self, _id):
		self.id = _id


def get_atom_index_by_label(label):
	"""Get atom index by label
	"""

	return ATOM_LABELS.index(label)


def map_pos_to_unit_space_0_1(pos):
	"""Map pos to space: [0, 1]
	"""

	return list(map(lambda p: (p + MAX_ATOM_POS) / (MAX_ATOM_POS * 2), pos))


def get_atom_ordinal_number_by_label(label):
	"""Get atom ordinal number by label
	"""

	return ATOM_ORDINAL_NUMBER[ATOM_LABELS.index(label)]


def get_molecule_mass(molecule):
	"""Get mass for each atom in a given molecule
	"""

	mass = []

	for i, atom in enumerate(molecule[4]):
		mass.append(ATOM_MASS[get_atom_index_by_label(atom[0])])

	total_mass = sum(mass)

	return mass, total_mass


def save_data_as_msgpack(data_dir_path, name_prefix, n):
	"""Save QM9 data in a pickle file
	"""

	data = load_data(data_dir_path, max_n = n)
	msgpack.pack(data, open(name_prefix + '_{}.msgpack'.format(len(data)), "wb"))


def load_data(directory, max_n = None):
	"""Load QM9 .xyz files, parse, and return QM9 item list
	"""

	file_list = glob.glob(os.path.join(directory, '*.xyz'))
	file_list.sort()

	if max_n is not None:
		file_list = file_list[: max_n]

	qm9_list = []

	for file in file_list:
		with open(file, 'r') as f:
			qm9_list.append(parse_qm9_item([line.strip().replace('*^', 'E') for line in f]))
			f.close()

	return qm9_list


def parse_qm9_item(data):
	"""Parse QM9 data file
	"""

	atom_n = int(data[0])
	gdb_id = int((data[1].split('\t')[0]).split()[1])
	properties = list(map(float, data[1].split('\t')[1:]))
	smiles = data[3 + atom_n].split('\t')
	atoms = []

	for i in range(2, 2 + atom_n):
		pos = list(map(float, data[i].split('\t')[1:4]))
		mulliken_charge = float(data[i].split('\t')[4])
		atom = data[i].split('\t')[0]
		atoms.append([atom, pos, mulliken_charge])

	return [gdb_id, atom_n, properties, smiles, atoms]


def get_property_targets(p_data, label):
	"""Get property values by property label
	"""

	p_index = QM9_PROPERTY_LABELS.index(label)
	p_target = []

	for item in p_data:
		p_target.append(item[p_index])

	return p_target


def get_min_max_property_value(data, label = None):
	"""Get min and max values for target property among the dataset.
	"""

	min_max = [1000000, -1000000]

	if label is not None:
		i = QM9_PROPERTY_LABELS.index(label)

		# Compute min max values

		for item in data:
			prop = item[2][i]
			min_max[0] = min(min_max[0], prop)
			min_max[1] = max(min_max[1], prop)
	else:

		min_max = [[1000000, -1000000] for _ in range(len(QM9_PROPERTY_LABELS))]

		# Compute min max values

		for item in data:
			for i, _label in enumerate(QM9_PROPERTY_LABELS):
				prop = item[2][i]
				min_max[i][0] = min(min_max[i][0], prop)
				min_max[i][1] = max(min_max[i][1], prop)

	return min_max


def transform_property_value_to_0_1_space(prop, label):
	"""Transform property value to unit space
	"""

	i = QM9_PROPERTY_LABELS.index(label)
	min_max = QM9_PROPERTY_MIN_MAX[i]
	norm = abs(min_max[0]) + min_max[1] if min_max[0] < 0 else min_max[1] - min_max[0]

	if type(prop) is list:
		for j, item in enumerate(prop):
			prop[j] = (item + (-1 * min_max[0])) / norm
	else:
		prop = (prop + (-1 * min_max[0])) / norm

	return prop


def transform_property_value_to_origin_space(prop, label):
	"""Transform property value from unit space to initial space
	"""

	i = QM9_PROPERTY_LABELS.index(label)
	min_max = QM9_PROPERTY_MIN_MAX[i]

	norm = abs(min_max[0]) + min_max[1] if min_max[0] < 0 else min_max[1] - min_max[0]

	if type(prop) is list:
		for j, item in enumerate(prop):
			prop[j] = item * norm - (-1 * min_max[0])
	else:
		prop = prop * norm - (-1 * min_max[0])

	return prop


def transform_mae_to_origin_space(prop, label):
	"""Transform property value from unit space to initial space
	"""

	i = QM9_PROPERTY_LABELS.index(label)
	min_max = QM9_PROPERTY_MIN_MAX[i]

	norm = abs(min_max[0]) + min_max[1] if min_max[0] < 0 else min_max[1] - min_max[0]

	if type(prop) is list:
		for j, item in enumerate(prop):
			prop[j] = item * norm
	else:
		prop = prop * norm

	return prop

