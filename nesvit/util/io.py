# Imports

from tensorflow.python.lib.io import file_io


def find_file_locally_and_move(file_name_prefix, file_target_dir):
	"""WORK-AROUND: Due to Google ML Cloud Service restrictions, models has to be moved to an external Bucket.
	"""

	files_list = file_io.list_directory("./")

	# Find local model file

	local_file = None

	for item in files_list:
		if file_name_prefix in item:
			local_file = item
			file_path = file_target_dir + item
			break

	# Copy local file to target directory

	if local_file is not None:
		with file_io.FileIO(local_file, mode = 'rb') as input_f:
			with file_io.FileIO(file_path, mode = 'wb+') as output_f:
				output_f.write(input_f.read())

		# Remove local file

		file_io.delete_file('./' + local_file)


def copy_file_to_local_dir(file_path, local_file):
	"""WORK-AROUND: Due to Google ML Cloud Service restrictions.
	"""

	with file_io.FileIO(file_path, mode = 'rb') as input_f:
		with file_io.FileIO(local_file, mode = 'wb+') as output_f:
			output_f.write(input_f.read())


def remove_file(file):
	"""Remove file
	"""

	file_io.delete_file(file)


def write_txt_file(file_prefix, _str):
	"""Write .txt file
	"""

	eval_file = open(file_prefix + ".txt", "wb+")
	eval_file.write(str.encode(_str))
	eval_file.close()

