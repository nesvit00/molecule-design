

def sort_model_paths(files):
	"""Sorting of strings with numerical aspect. Sort by first number section.
		Example filename: m_smiles_prop_01_..., m_smiles_prop_121_
	"""

	max_n_positions = 10

	# Remove log files from list

	files = [x for x in files if 'm_' in x[:2]]

	model_prefix = get_model_prefix(files[0])

	def keyfunction(x):
		a = x[len(model_prefix):]
		stop_index = 0
		for i in range(len(a)):
			if '_' in a[i]:
				stop_index = i
				break
		n = str(int(a[:stop_index]))
		zero_padding = ''
		for _ in range(max_n_positions - len(n)):
			zero_padding += '0'
		return (zero_padding + '%d') % int(n)

	return sorted(files, key = keyfunction)


def get_model_prefix(model_name):
	"""Get Model prefix
	"""

	stop_index = 0
	for i in range(len(model_name)):
		if model_name[i].isdigit():
			stop_index = i
			break

	return model_name[:stop_index]

