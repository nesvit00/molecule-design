# Imports

import keras
import msgpack
from tensorflow.python.lib.io import file_io


class MoveCheckpoint(keras.callbacks.Callback):
	def __init__(self, file_dir, model_prefix):
		super().__init__()
		self.log_prefix = 'log_' + model_prefix + '_'
		self.file_dir = file_dir + '/'
		self.model_prefix = model_prefix
		self.history = []

	def on_epoch_end(self, epoch, logs = {}):
		"""Epoch end training event
		"""

		self.history.append(logs)

		# Move model file

		self.find_file_locally_and_move(self.model_prefix, self.file_dir)

		# Save log file locally

		msgpack.pack(str(self.history), open(self.log_prefix + str(epoch + 1) + '.msgpack', "wb+"))

		# Move log file

		self.remove_file(self.log_prefix, self.file_dir)
		self.find_file_locally_and_move(self.log_prefix, self.file_dir)

	def remove_file(self, file_name_prefix, file_target_dir):
		"""Remove file
		"""

		files_list = file_io.list_directory(file_target_dir)

		# Find local model file

		file_path = None

		for item in files_list:
			if file_name_prefix in item:
				file_path = file_target_dir + item
				break

		if file_path is not None:
			file_io.delete_file(file_path)

	def find_file_locally_and_move(self, file_name_prefix, file_target_dir):
		"""WORK-AROUND: Due to Google ML Cloud Service restrictions, models has to be moved to an external Bucket.
		"""

		files_list = file_io.list_directory("./")

		# Find local model file

		local_file = None

		for item in files_list:
			if file_name_prefix in item:
				local_file = item
				file_path = file_target_dir + item
				break

		# Copy local file to target directory

		if local_file is not None:
			with file_io.FileIO(local_file, mode = 'rb') as input_f:
				with file_io.FileIO(file_path, mode = 'wb+') as output_f:
					output_f.write(input_f.read())

			# Remove local file

			file_io.delete_file('./' + local_file)

