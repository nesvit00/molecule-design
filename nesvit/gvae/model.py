import tensorflow as tf
import keras.backend as backend

from keras import objectives
from keras.models import Model
from keras.layers import *


class GrammarModel(object):
	"""Grammar VAE model
	"""

	def __init__(self, max_production_seq_len, prod_rule_n, latent_dim, rule_index, mask, model_file = None):
		"""Initialize grammar model
		"""

		# Init variable

		self.rule_index_keras = K.variable(rule_index)
		self.mask_keras = K.variable(mask)

		# Build model components

		x = Input(shape = (max_production_seq_len, prod_rule_n))
		_z = Input(shape = (latent_dim,))
		x_vae = Input(shape = (max_production_seq_len, prod_rule_n))

		_, z = self.build_encoder(x, max_production_seq_len, prod_rule_n, latent_dim)
		_x = self.build_decoder(_z, max_production_seq_len, prod_rule_n, latent_dim)

		loss_vae, z_vae = self.build_encoder(x_vae, max_production_seq_len, prod_rule_n, latent_dim)
		_x_vae = self.build_decoder(z_vae, max_production_seq_len, prod_rule_n, latent_dim)

		x_encoder_only = Input(shape = (max_production_seq_len, prod_rule_n))
		z_mean, z_var = self.build_encoder_only(x_encoder_only, latent_dim)

		# Compile models

		self.encoder = Model(x, z)
		self.decoder = Model(_z, _x)
		self.vae = Model(x_vae, _x_vae)
		self.encoder_only = Model(inputs = x_encoder_only, outputs = [z_mean, z_var])

		if model_file is not None:
			self.vae.load_weights(model_file)
			self.encoder.load_weights(model_file, by_name = True)
			self.decoder.load_weights(model_file, by_name = True)
			self.encoder_only.load_weights(model_file, by_name = True)

		self.vae.compile(optimizer = 'Adam', loss = loss_vae, metrics = ['accuracy'])

	def build_encoder_only(self, encoder_input, latent_dim):
		"""Build mean encoder
		"""

		encoder_branch = Convolution1D(12, 8, name = 'encoder_conv_1', activation = 'relu')(encoder_input)
		encoder_branch = Convolution1D(12, 8, name = 'encoder_conv_2', activation = 'relu')(encoder_branch)
		encoder_branch = Convolution1D(12, 8, name = 'encoder_conv_3', activation = 'relu')(encoder_branch)
		encoder_branch = Flatten(name = 'encoder_flatten_1')(encoder_branch)
		encoder_branch = Dense(256, name = 'encoder_dense_1', activation = 'relu')(encoder_branch)

		encoder_mean = Dense(latent_dim, name = 'encoder_mean', activation = 'linear')(encoder_branch)
		encoder_var = Dense(latent_dim, name = 'encoder_var', activation = 'linear')(encoder_branch)

		return encoder_mean, encoder_var

	def build_encoder(self, encoder_input, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build encoder
		"""

		encoder_sample_mean = 0.0
		encoder_sample_var = 0.1

		def encoder_sampler(args):
			"""Sampling z from the distribution Q(z|X)
			"""

			mean, log_var = args
			batch_size = K.shape(mean)[0]
			epsilon = K.random_normal(shape = (batch_size, latent_dim), mean = encoder_sample_mean, stddev = encoder_sample_var)

			return mean + K.exp(log_var / 2) * epsilon

		def loss(x, _x):
			"""Variational Autoencoder loss function, where x is initial input and _x is predicted.
			"""

			# Parse production rules from the output values

			indices_of_max_value = tf.reshape(K.argmax(x), [-1])
			rule_index_ = tf.expand_dims(tf.gather(self.rule_index_keras, indices_of_max_value), 1)

			masked_prod_rule = tf.gather_nd(self.mask_keras, tf.cast(rule_index_, tf.int32))
			masked_prod_rule = tf.reshape(masked_prod_rule, [-1, max_production_seq_len, prod_rule_n])

			# Sampling from masked logit (see Equation 5)

			exp = tf.multiply(K.exp(_x), masked_prod_rule)
			norm = K.sum(exp, axis = -1, keepdims = True)
			_x = tf.div(exp, norm)

			# Flatten input and reconstruction

			x = K.flatten(x)
			_x = K.flatten(_x)

			# Compute loss values

			reconstruction_loss = max_production_seq_len * objectives.binary_crossentropy(x, _x)
			divergence_loss = 0.5 * K.mean(K.exp(encoder_var) + K.square(encoder_mean) - encoder_var - 1, axis = -1)

			# Return reconstruction loss + divergence loss

			return reconstruction_loss + divergence_loss

		encoder_branch = Convolution1D(12, 8, name = 'encoder_conv_1', activation = 'relu')(encoder_input)
		encoder_branch = Convolution1D(12, 8, name = 'encoder_conv_2', activation = 'relu')(encoder_branch)
		encoder_branch = Convolution1D(12, 8, name = 'encoder_conv_3', activation = 'relu')(encoder_branch)
		encoder_branch = Flatten(name = 'encoder_flatten_1')(encoder_branch)
		encoder_branch = Dense(256, name = 'encoder_dense_1', activation = 'relu')(encoder_branch)

		encoder_mean = Dense(latent_dim, name = 'encoder_mean', activation = 'linear')(encoder_branch)
		encoder_var = Dense(latent_dim, name = 'encoder_var', activation = 'linear')(encoder_branch)

		return (loss,
				Lambda(encoder_sampler, output_shape = (latent_dim,), name = 'encoder_latent')([encoder_mean, encoder_var]))

	def build_decoder(self, decoder_input, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build decoder
		"""

		decoder_branch = Dense(latent_dim, name = 'decoder_dense_1', activation = 'relu')(decoder_input)
		decoder_branch = RepeatVector(max_production_seq_len, name = 'repeat_vector')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_1')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_2')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_3')(decoder_branch)

		decoder_output = TimeDistributed(Dense(prod_rule_n), name = 'decoder_output')(decoder_branch)

		return decoder_output


class BoB2GrammarModel(object):
	"""Grammar VAE model
	"""

	def __init__(self, cond_input_len, max_production_seq_len, prod_rule_n, latent_dim, rule_index, mask, model_file = None):
		"""Initialize grammar model
		"""

		# Init variable

		self.rule_index_keras = K.variable(rule_index)
		self.mask_keras = K.variable(mask)

		# Build Encoder

		x = Input(shape = (cond_input_len,))
		_, z = self.build_encoder(x, cond_input_len, max_production_seq_len, prod_rule_n, latent_dim)

		# Build Decoder

		_z = Input(shape = (latent_dim,))
		_x = self.build_decoder(_z, max_production_seq_len, prod_rule_n, latent_dim)

		# Build VAE

		x_vae = Input(shape = (cond_input_len,))
		loss_vae, z_vae = self.build_encoder(x_vae, cond_input_len, max_production_seq_len, prod_rule_n, latent_dim)
		_x_vae = self.build_decoder(z_vae, max_production_seq_len, prod_rule_n, latent_dim)

		# Build Mean Encoder

		x_encoder_only = Input(shape = (cond_input_len,))
		z_mean, z_var = self.build_encoder_only(x_encoder_only, cond_input_len, latent_dim)

		# Compile models

		self.encoder = Model(x, z)
		self.decoder = Model(_z, _x)
		self.vae = Model(x_vae, _x_vae)
		self.encoder_only = Model(inputs = x_encoder_only, outputs = [z_mean, z_var])

		if model_file is not None:
			self.vae.load_weights(model_file)
			self.encoder.load_weights(model_file, by_name = True)
			self.decoder.load_weights(model_file, by_name = True)
			self.encoder_only.load_weights(model_file, by_name = True)

		self.vae.compile(optimizer = 'Adam', loss = loss_vae, metrics = ['accuracy'])

	def build_encoder_only(self, encoder_input, cond_input_len, latent_dim):
		"""Build mean encoder
		"""

		encoder_branch = Dense(512, name = 'encoder_dense_1', activation = 'relu')(encoder_input)
		encoder_branch = Dense(512, name = 'encoder_dense_2', activation = 'relu')(encoder_branch)
		encoder_branch = Dense(512, name = 'encoder_dense_3', activation = 'relu')(encoder_branch)
		encoder_branch = Dense(256, name = 'encoder_dense_4', activation = 'relu')(encoder_branch)

		encoder_mean = Dense(latent_dim, name = 'encoder_mean', activation = 'linear')(encoder_branch)
		encoder_var = Dense(latent_dim, name = 'encoder_var', activation = 'linear')(encoder_branch)

		return encoder_mean, encoder_var

	def build_encoder(self, encoder_input, cond_input_len, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build encoder
		"""

		encoder_sample_mean = 0.0
		encoder_sample_var = 0.1

		def encoder_sampler(args):
			"""Sampling z from the distribution Q(z|X)
			"""

			mean, log_var = args
			batch_size = K.shape(mean)[0]
			epsilon = K.random_normal(shape = (batch_size, latent_dim), mean = encoder_sample_mean, stddev = encoder_sample_var)

			return mean + K.exp(log_var / 2) * epsilon

		def loss(x, _x):
			"""Variational Autoencoder loss function, where x is initial input and _x is predicted.
			"""

			# Parse production rules from the output values

			indices_of_max_value = tf.reshape(K.argmax(x), [-1])
			rule_index_ = tf.expand_dims(tf.gather(self.rule_index_keras, indices_of_max_value), 1)

			masked_prod_rule = tf.gather_nd(self.mask_keras, tf.cast(rule_index_, tf.int32))
			masked_prod_rule = tf.reshape(masked_prod_rule, [-1, max_production_seq_len, prod_rule_n])

			# Sampling from masked logit (see Equation 5)

			exp = tf.multiply(K.exp(_x), masked_prod_rule)
			norm = K.sum(exp, axis = -1, keepdims = True)
			_x = tf.div(exp, norm)

			# Flatten input and reconstruction

			x = K.flatten(x)
			_x = K.flatten(_x)

			# Compute loss values

			reconstruction_loss = max_production_seq_len * objectives.binary_crossentropy(x, _x)
			divergence_loss = 0.5 * K.mean(K.exp(encoder_var) + K.square(encoder_mean) - encoder_var - 1, axis = -1)

			# Return reconstruction loss + divergence loss

			return reconstruction_loss + divergence_loss

		encoder_branch = Dense(512, name = 'encoder_dense_1', activation = 'relu')(encoder_input)
		encoder_branch = Dense(512, name = 'encoder_dense_2', activation = 'relu')(encoder_branch)
		encoder_branch = Dense(512, name = 'encoder_dense_3', activation = 'relu')(encoder_branch)
		encoder_branch = Dense(256, name = 'encoder_dense_4', activation = 'relu')(encoder_branch)

		encoder_mean = Dense(latent_dim, name = 'encoder_mean', activation = 'linear')(encoder_branch)
		encoder_var = Dense(latent_dim, name = 'encoder_var', activation = 'linear')(encoder_branch)

		return (loss,
				Lambda(encoder_sampler, output_shape = (latent_dim,), name = 'encoder_latent')([encoder_mean, encoder_var]))

	def build_decoder(self, decoder_input, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build decoder
		"""

		decoder_branch = Dense(latent_dim, name = 'decoder_dense_1', activation = 'relu')(decoder_input)
		decoder_branch = RepeatVector(max_production_seq_len, name = 'repeat_vector')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_1')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_2')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_3')(decoder_branch)

		decoder_output = TimeDistributed(Dense(prod_rule_n), name = 'decoder_output')(decoder_branch)

		return decoder_output


class SMILES2GrammarModel(object):
	"""Grammar VAE model
	"""

	def __init__(self, cond_input_len, cond_character_len, max_production_seq_len, prod_rule_n, latent_dim, rule_index, mask, model_file = None):
		"""Initialize grammar model
		"""

		# Init variable

		self.rule_index_keras = K.variable(rule_index)
		self.mask_keras = K.variable(mask)

		# Build Encoder

		x = Input(shape = (cond_input_len, cond_character_len))
		_, z = self.build_encoder(x, cond_input_len, max_production_seq_len, prod_rule_n, latent_dim)

		# Build Decoder

		_z = Input(shape = (latent_dim,))
		_x = self.build_decoder(_z, max_production_seq_len, prod_rule_n, latent_dim)

		# Build VAE

		x_vae = Input(shape = (cond_input_len, cond_character_len))
		loss_vae, z_vae = self.build_encoder(x_vae, cond_input_len, max_production_seq_len, prod_rule_n, latent_dim)
		_x_vae = self.build_decoder(z_vae, max_production_seq_len, prod_rule_n, latent_dim)

		# Build Mean Encoder

		x_encoder_only = Input(shape = (cond_input_len, cond_character_len))
		z_mean, z_var = self.build_encoder_only(x_encoder_only, cond_input_len, latent_dim)

		# Compile models

		self.encoder = Model(x, z)
		self.decoder = Model(_z, _x)
		self.vae = Model(x_vae, _x_vae)
		self.encoder_only = Model(inputs = x_encoder_only, outputs = [z_mean, z_var])

		if model_file is not None:
			self.vae.load_weights(model_file)
			self.encoder.load_weights(model_file, by_name = True)
			self.decoder.load_weights(model_file, by_name = True)
			self.encoder_only.load_weights(model_file, by_name = True)

		self.vae.compile(optimizer = 'Adam', loss = loss_vae, metrics = ['accuracy'])

	def build_encoder_only(self, encoder_input, cond_input_len, latent_dim):
		"""Build mean encoder
		"""

		encoder_branch = LSTM(128, name = 'encoder_lstm_1', return_sequences = True)(encoder_input)
		encoder_branch = LSTM(64, name = 'encoder_lstm_2', return_sequences = False)(encoder_branch)

		encoder_mean = Dense(latent_dim, name = 'encoder_mean', activation = 'linear')(encoder_branch)
		encoder_var = Dense(latent_dim, name = 'encoder_var', activation = 'linear')(encoder_branch)

		return encoder_mean, encoder_var

	def build_encoder(self, encoder_input, cond_input_len, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build encoder
		"""

		encoder_sample_mean = 0.0
		encoder_sample_var = 0.1

		def encoder_sampler(args):
			"""Sampling z from the distribution Q(z|X)
			"""

			mean, log_var = args
			batch_size = K.shape(mean)[0]
			epsilon = K.random_normal(shape = (batch_size, latent_dim), mean = encoder_sample_mean, stddev = encoder_sample_var)

			return mean + K.exp(log_var / 2) * epsilon

		def loss(x, _x):
			"""Variational Autoencoder loss function, where x is initial input and _x is predicted.
			"""

			# Parse production rules from the output values

			indices_of_max_value = tf.reshape(K.argmax(x), [-1])
			rule_index_ = tf.expand_dims(tf.gather(self.rule_index_keras, indices_of_max_value), 1)

			masked_prod_rule = tf.gather_nd(self.mask_keras, tf.cast(rule_index_, tf.int32))
			masked_prod_rule = tf.reshape(masked_prod_rule, [-1, max_production_seq_len, prod_rule_n])

			# Sampling from masked logit (see Equation 5)

			exp = tf.multiply(K.exp(_x), masked_prod_rule)
			norm = K.sum(exp, axis = -1, keepdims = True)
			_x = tf.div(exp, norm)

			# Flatten input and reconstruction

			x = K.flatten(x)
			_x = K.flatten(_x)

			# Compute loss values

			reconstruction_loss = max_production_seq_len * objectives.binary_crossentropy(x, _x)
			divergence_loss = 0.5 * K.mean(K.exp(encoder_var) + K.square(encoder_mean) - encoder_var - 1, axis = -1)

			# Return reconstruction loss + divergence loss

			return reconstruction_loss + divergence_loss

		encoder_branch = LSTM(128, name = 'encoder_lstm_1', return_sequences = True)(encoder_input)
		encoder_branch = LSTM(64, name = 'encoder_lstm_2', return_sequences = False)(encoder_branch)

		encoder_mean = Dense(latent_dim, name = 'encoder_mean', activation = 'linear')(encoder_branch)
		encoder_var = Dense(latent_dim, name = 'encoder_var', activation = 'linear')(encoder_branch)

		return (loss,
				Lambda(encoder_sampler, output_shape = (latent_dim,), name = 'encoder_latent')([encoder_mean, encoder_var]))

	def build_decoder(self, decoder_input, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build decoder
		"""

		decoder_branch = Dense(latent_dim, name = 'decoder_dense_1', activation = 'relu')(decoder_input)
		decoder_branch = RepeatVector(max_production_seq_len, name = 'repeat_vector')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_1')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_2')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_3')(decoder_branch)

		decoder_output = TimeDistributed(Dense(prod_rule_n), name = 'decoder_output')(decoder_branch)

		return decoder_output


class SMILES2GrammarRegressorModel(object):
	"""Grammar VAE model
	"""

	def __init__(self, prop_vector_len, cond_input_len, cond_character_len, max_production_seq_len, prod_rule_n, latent_dim, rule_index, mask, model_file = None):
		"""Initialize grammar model
		"""

		# Init variable

		self.rule_index_keras = K.variable(rule_index)
		self.mask_keras = K.variable(mask)

		# Build auxiliary Models x = Input(shape = (cond_input_len, cond_character_len))

		self.encoder = self.build_encoder(cond_input_len, cond_character_len, latent_dim)
		self.decoder = self.build_decoder(max_production_seq_len, prod_rule_n, latent_dim)
		self.predictor = self.build_predictor(prop_vector_len, latent_dim)

		# Build VAE Model

		self.vae, loss = self.build_vae(max_production_seq_len, prod_rule_n, latent_dim)

		# Compile Model

		if model_file is not None:
			self.vae.load_weights(model_file)
			self.encoder.load_weights(model_file, by_name = True)
			self.decoder.load_weights(model_file, by_name = True)
			self.predictor.load_weights(model_file, by_name = True)

		model_losses = {'y_out': loss, 'prop_pred': 'mse'}
		model_metrics = {'y_out': ['accuracy'], 'prop_pred': ['mae', 'mean_absolute_percentage_error']}
		self.vae.compile(optimizer = 'Adam', loss = model_losses, metrics = model_metrics)

	def identity(self, args):
		"""Identity function
		"""

		return args

	def build_encoder(self, cond_input_len, cond_character_len, latent_dim):
		"""Build mean encoder
		"""

		encoder_input = Input(shape = (cond_input_len, cond_character_len), name = 'encoder_input')
		encoder_branch = LSTM(128, name = 'encoder_lstm_1', return_sequences = True)(encoder_input)
		encoder_branch = LSTM(64, name = 'encoder_lstm_2', return_sequences = False)(encoder_branch)

		encoder_mean = Dense(latent_dim, name = 'encoder_mean', activation = 'linear')(encoder_branch)
		encoder_var = Dense(latent_dim, name = 'encoder_var', activation = 'linear')(encoder_branch)

		return Model(encoder_input, [encoder_mean, encoder_var], name = 'encoder')

	def build_decoder(self, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build decoder
		"""

		decoder_input = Input(shape = (latent_dim,), name = 'decoder_input')
		decoder_branch = Dense(latent_dim, name = 'decoder_dense_1', activation = 'relu')(decoder_input)
		decoder_branch = RepeatVector(max_production_seq_len, name = 'repeat_vector')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_1')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_2')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_3')(decoder_branch)

		decoder_output = TimeDistributed(Dense(prod_rule_n), name = 'decoder_output')(decoder_branch)

		return Model(decoder_input, decoder_output, name = 'decoder')

	def build_predictor(self, prop_vector_len, latent_dim):
		"""Build property predictor
		"""

		predictor_input = Input(shape = (latent_dim,), name = 'predictor_input')
		predictor_branch = Dense(128, name = 'regressor_dense_1', activation = 'linear')(predictor_input)
		predictor_branch = LeakyReLU(alpha = 0.01)(predictor_branch)
		predictor_branch = Dense(64, name = 'regressor_dense_2', activation = 'linear')(predictor_branch)
		predictor_branch = LeakyReLU(alpha = 0.01)(predictor_branch)
		predictor_output = Dense(1, name = 'regressor_output', activation = 'sigmoid')(predictor_branch)

		return Model(predictor_input, predictor_output, name = "predictor")

	def build_vae(self, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build VAE framework
		"""

		def loss(x, _x):
			"""Variational Autoencoder loss function, where x is initial input and _x is predicted.
			"""

			# Parse production rules from the output values

			indices_of_max_value = tf.reshape(K.argmax(x), [-1])
			rule_index_ = tf.expand_dims(tf.gather(self.rule_index_keras, indices_of_max_value), 1)

			masked_prod_rule = tf.gather_nd(self.mask_keras, tf.cast(rule_index_, tf.int32))
			masked_prod_rule = tf.reshape(masked_prod_rule, [-1, max_production_seq_len, prod_rule_n])

			# Sampling from masked logit (see Equation 5)

			exp = tf.multiply(K.exp(_x), masked_prod_rule)
			norm = K.sum(exp, axis = -1, keepdims = True)
			_x = tf.div(exp, norm)

			# Flatten input and reconstruction

			x = K.flatten(x)
			_x = K.flatten(_x)

			# Compute loss values

			reconstruction_loss = max_production_seq_len * objectives.binary_crossentropy(x, _x)
			divergence_loss = 0.5 * K.mean(K.exp(encoder_var) + K.square(encoder_mean) - encoder_var - 1, axis = -1)

			# Return reconstruction loss + divergence loss

			return reconstruction_loss + divergence_loss

		x_inputs = self.encoder.inputs[0]
		encoder_mean, encoder_var = self.encoder(x_inputs)
		z_sample = self.vae_layers(encoder_mean, encoder_var, latent_dim)

		x_recon = self.decoder(z_sample)
		x_recon = Lambda(self.identity, name = 'y_out')(x_recon)

		prop_pred = self.predictor(encoder_mean)
		prop_pred = Lambda(self.identity, name = 'prop_pred')(prop_pred)

		return Model(x_inputs, [x_recon, prop_pred]), loss

	def vae_layers(self, encoder_mean, encoder_var, latent_dim):
		"""Build encoder
		"""

		encoder_sample_mean = 0.0
		encoder_sample_var = 0.1

		def encoder_sampler(args):
			"""Sampling z from the distribution Q(z|X)
			"""

			mean, log_var = args
			batch_size = K.shape(mean)[0]
			epsilon = K.random_normal(shape = (batch_size, latent_dim), mean = encoder_sample_mean, stddev = encoder_sample_var)

			return mean + K.exp(log_var / 2) * epsilon

		z_sample = Lambda(encoder_sampler, output_shape = (latent_dim,), name = 'encoder_latent')([encoder_mean, encoder_var])

		return z_sample


class BoB2GrammarRegressorModel(object):
	"""Grammar VAE model
	"""

	def __init__(self, prop_vector_len, cond_input_len, max_production_seq_len, prod_rule_n, latent_dim, rule_index, mask, model_file = None):
		"""Initialize grammar model
		"""

		# Init variable

		self.rule_index_keras = K.variable(rule_index)
		self.mask_keras = K.variable(mask)

		# Build auxiliary Models

		self.encoder = self.build_encoder(cond_input_len, latent_dim)
		self.decoder = self.build_decoder(max_production_seq_len, prod_rule_n, latent_dim)
		self.predictor = self.build_predictor(prop_vector_len, latent_dim)

		# Build VAE Model

		self.vae, loss = self.build_vae(max_production_seq_len, prod_rule_n, latent_dim)

		# Compile Model

		if model_file is not None:
			self.vae.load_weights(model_file)
			self.encoder.load_weights(model_file, by_name = True)
			self.decoder.load_weights(model_file, by_name = True)
			self.predictor.load_weights(model_file, by_name = True)

		model_losses = {'y_out': loss, 'prop_pred': 'mse'}
		model_metrics = {'y_out': ['accuracy'], 'prop_pred': ['mae', 'mean_absolute_percentage_error']}
		self.vae.compile(optimizer = 'Adam', loss = model_losses, metrics = model_metrics)

	def identity(self, args):
		"""Identity function
		"""

		return args

	def build_encoder(self, cond_input_len, latent_dim):
		"""Build mean encoder
		"""

		encoder_input = Input(shape = (cond_input_len,), name = 'encoder_input')
		encoder_branch = Dense(512, name = 'encoder_dense_1', activation = 'relu')(encoder_input)
		encoder_branch = Dense(512, name = 'encoder_dense_2', activation = 'relu')(encoder_branch)
		encoder_branch = Dense(256, name = 'encoder_dense_3', activation = 'relu')(encoder_branch)
		encoder_branch = Dense(256, name = 'encoder_dense_4', activation = 'relu')(encoder_branch)

		encoder_mean = Dense(latent_dim, name = 'encoder_mean', activation = 'linear')(encoder_branch)
		encoder_var = Dense(latent_dim, name = 'encoder_var', activation = 'linear')(encoder_branch)

		return Model(encoder_input, [encoder_mean, encoder_var], name = 'encoder')

	def build_decoder(self, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build decoder
		"""

		decoder_input = Input(shape = (latent_dim,), name = 'decoder_input')
		decoder_branch = Dense(latent_dim, name = 'decoder_dense_1', activation = 'relu')(decoder_input)
		decoder_branch = RepeatVector(max_production_seq_len, name = 'repeat_vector')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_1')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_2')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_3')(decoder_branch)

		decoder_output = TimeDistributed(Dense(prod_rule_n), name = 'decoder_output')(decoder_branch)

		return Model(decoder_input, decoder_output, name = 'decoder')

	def build_predictor(self, prop_vector_len, latent_dim):
		"""Build property predictor
		"""

		predictor_input = Input(shape = (latent_dim,), name = 'predictor_input')
		predictor_branch = Dense(128, name = 'regressor_dense_1', activation = 'linear')(predictor_input)
		predictor_branch = LeakyReLU(alpha = 0.01)(predictor_branch)
		predictor_branch = Dense(64, name = 'regressor_dense_2', activation = 'linear')(predictor_branch)
		predictor_branch = LeakyReLU(alpha = 0.01)(predictor_branch)
		predictor_output = Dense(1, name = 'regressor_output', activation = 'sigmoid')(predictor_branch)

		return Model(predictor_input, predictor_output, name = "predictor")

	def build_vae(self, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build VAE framework
		"""

		def loss(x, _x):
			"""Variational Autoencoder loss function, where x is initial input and _x is predicted.
			"""

			# Parse production rules from the output values

			indices_of_max_value = tf.reshape(K.argmax(x), [-1])
			rule_index_ = tf.expand_dims(tf.gather(self.rule_index_keras, indices_of_max_value), 1)

			masked_prod_rule = tf.gather_nd(self.mask_keras, tf.cast(rule_index_, tf.int32))
			masked_prod_rule = tf.reshape(masked_prod_rule, [-1, max_production_seq_len, prod_rule_n])

			# Sampling from masked logit (see Equation 5)

			exp = tf.multiply(K.exp(_x), masked_prod_rule)
			norm = K.sum(exp, axis = -1, keepdims = True)
			_x = tf.div(exp, norm)

			# Flatten input and reconstruction

			x = K.flatten(x)
			_x = K.flatten(_x)

			# Compute loss values

			reconstruction_loss = max_production_seq_len * objectives.binary_crossentropy(x, _x)
			divergence_loss = 0.5 * K.mean(K.exp(encoder_var) + K.square(encoder_mean) - encoder_var - 1, axis = -1)

			# Return reconstruction loss + divergence loss

			return reconstruction_loss + divergence_loss

		x_inputs = self.encoder.inputs[0]
		encoder_mean, encoder_var = self.encoder(x_inputs)
		z_sample = self.vae_layers(encoder_mean, encoder_var, latent_dim)

		x_recon = self.decoder(z_sample)
		x_recon = Lambda(self.identity, name = 'y_out')(x_recon)

		prop_pred = self.predictor(encoder_mean)
		prop_pred = Lambda(self.identity, name = 'prop_pred')(prop_pred)

		return Model(x_inputs, [x_recon, prop_pred]), loss

	def vae_layers(self, encoder_mean, encoder_var, latent_dim):
		"""Build encoder
		"""

		encoder_sample_mean = 0.0
		encoder_sample_var = 0.1

		def encoder_sampler(args):
			"""Sampling z from the distribution Q(z|X)
			"""

			mean, log_var = args
			batch_size = K.shape(mean)[0]
			epsilon = K.random_normal(shape = (batch_size, latent_dim), mean = encoder_sample_mean, stddev = encoder_sample_var)

			return mean + K.exp(log_var / 2) * epsilon

		z_sample = Lambda(encoder_sampler, output_shape = (latent_dim,), name = 'encoder_latent')([encoder_mean, encoder_var])

		return z_sample


class GrammarModelWithPropertyPrediction(object):
	"""Grammar VAE model with property prediction
	"""

	def __init__(self, max_production_seq_len, prod_rule_n, latent_dim, rule_index, mask, model_file = None):
		"""Initialize grammar model
		"""

		# Init variable

		self.rule_index_keras = K.variable(rule_index)
		self.mask_keras = K.variable(mask)

		# Build auxiliary Models

		self.encoder = self.build_encoder(max_production_seq_len, prod_rule_n, latent_dim)
		self.decoder = self.build_decoder(max_production_seq_len, prod_rule_n, latent_dim)
		self.predictor = self.build_predictor(latent_dim)

		# Build VAE Model

		self.vae, loss = self.build_vae(max_production_seq_len, prod_rule_n, latent_dim)

		# Compile Model

		if model_file is not None:
			self.vae.load_weights(model_file)
			self.encoder.load_weights(model_file, by_name = True)
			self.decoder.load_weights(model_file, by_name = True)
			self.predictor.load_weights(model_file, by_name = True)

		model_losses = {'y_out': loss, 'prop_pred': 'mse'}
		model_metrics = {'y_out': ['accuracy'], 'prop_pred': ['mae', 'mean_absolute_percentage_error']}
		self.vae.compile(optimizer = 'Adam', loss = model_losses, loss_weights = [1.0, 35000.0], metrics = model_metrics)

	def identity(self, args):
		"""Identity function
		"""

		return args

	def build_encoder(self, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build encoder
		"""

		encoder_input = Input(shape = (max_production_seq_len, prod_rule_n), name = 'encoder_input')
		encoder_branch = Convolution1D(12, 8, name = 'encoder_conv_1', activation = 'relu')(encoder_input)
		encoder_branch = Convolution1D(12, 8, name = 'encoder_conv_2', activation = 'relu')(encoder_branch)
		encoder_branch = Convolution1D(12, 8, name = 'encoder_conv_3', activation = 'relu')(encoder_branch)
		encoder_branch = Flatten(name = 'encoder_flatten_1')(encoder_branch)
		encoder_branch = Dense(256, name = 'encoder_dense_1', activation = 'relu')(encoder_branch)

		encoder_mean = Dense(latent_dim, name = 'encoder_mean', activation = 'linear')(encoder_branch)
		encoder_var = Dense(latent_dim, name = 'encoder_var', activation = 'linear')(encoder_branch)

		return Model(encoder_input, [encoder_mean, encoder_var], name = 'encoder')

	def build_decoder(self, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build decoder
		"""

		decoder_input = Input(shape = (latent_dim,), name = 'decoder_input')
		decoder_branch = Dense(latent_dim, name = 'decoder_dense_1', activation = 'relu')(decoder_input)
		decoder_branch = RepeatVector(max_production_seq_len, name = 'repeat_vector')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_1')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_2')(decoder_branch)
		decoder_branch = GRU(256, return_sequences = True, name = 'decoder_gru_3')(decoder_branch)

		decoder_output = TimeDistributed(Dense(prod_rule_n), name = 'decoder_output')(decoder_branch)

		return Model(decoder_input, decoder_output, name = 'y_out')

	def build_predictor(self, latent_dim):
		"""Build property predictor
		"""

		predictor_input = Input(shape = (latent_dim,), name = 'predictor_input')
		predictor_branch = Dense(128, name = 'regressor_dense_1', activation = 'relu')(predictor_input)
		predictor_branch = Dense(128, name = 'regressor_dense_2', activation = 'relu')(predictor_branch)
		predictor_output = Dense(1, name = 'regressor_output', activation = 'sigmoid')(predictor_branch)

		return Model(predictor_input, predictor_output, name = "prop_pred")

	def build_vae(self, max_production_seq_len, prod_rule_n, latent_dim):
		"""Build VAE framework
		"""

		def loss(x, _x):
			"""Variational Autoencoder loss function, where x is initial input and _x is predicted.
			"""

			# Parse production rules from the output values

			indices_of_max_value = tf.reshape(K.argmax(x), [-1])
			rule_index_ = tf.expand_dims(tf.gather(self.rule_index_keras, indices_of_max_value), 1)

			masked_prod_rule = tf.gather_nd(self.mask_keras, tf.cast(rule_index_, tf.int32))
			masked_prod_rule = tf.reshape(masked_prod_rule, [-1, max_production_seq_len, prod_rule_n])

			# Sampling from masked logit (see Equation 5)

			exp = tf.multiply(K.exp(_x), masked_prod_rule)
			norm = K.sum(exp, axis = -1, keepdims = True)
			_x = tf.div(exp, norm)

			# Flatten input and reconstruction

			x = K.flatten(x)
			_x = K.flatten(_x)

			# Compute loss values

			reconstruction_loss = max_production_seq_len * objectives.binary_crossentropy(x, _x)
			divergence_loss = 0.5 * K.mean(K.exp(encoder_var) + K.square(encoder_mean) - encoder_var - 1, axis = -1)

			# Return reconstruction loss + divergence loss

			return reconstruction_loss + divergence_loss

		x_inputs = self.encoder.inputs[0]
		encoder_mean, encoder_var = self.encoder(x_inputs)
		z_sample = self.vae_layers(encoder_mean, encoder_var, latent_dim)

		x_recon = self.decoder(z_sample)
		prop_pred = self.predictor(encoder_mean)

		return Model(x_inputs, [x_recon, prop_pred]), loss

	def vae_layers(self, encoder_mean, encoder_var, latent_dim):
		"""Build encoder
		"""

		encoder_sample_mean = 0.0
		encoder_sample_var = 0.1

		def encoder_sampler(args):
			"""Sampling z from the distribution Q(z|X)
			"""

			mean, log_var = args
			batch_size = K.shape(mean)[0]
			epsilon = K.random_normal(shape = (batch_size, latent_dim), mean = encoder_sample_mean, stddev = encoder_sample_var)

			return mean + K.exp(log_var / 2) * epsilon

		z_sample = Lambda(encoder_sampler, output_shape = (latent_dim,), name = 'encoder_latent')([encoder_mean, encoder_var])

		return z_sample

