import nltk
import pickle
import numpy as np
import msgpack


class GrammarParser(object):

	def __init__(self, max_prod_tree_n):
		"""QM9 SMILES grammar parser
		"""

		self.grammar_str = """smiles -> chain
				chain -> branched_atom
				chain -> chain branched_atom
				chain -> chain bond branched_atom
				branched_atom -> atom
				branched_atom -> atom RB
				branched_atom -> atom BB
				branched_atom -> atom RB BB
				atom -> bracket_atom
				atom -> aliphatic_organic
				atom -> aromatic_organic
				RB -> RB ringbond
				RB -> ringbond
				BB -> BB branch
				BB -> branch
				ringbond -> DIGIT
				ringbond -> bond DIGIT
				branch -> '(' chain ')'
				branch -> '(' bond chain ')'
				bracket_atom -> '[' BAI ']'
				BAI -> isotope symbol BAC
				BAI -> symbol BAC
				BAI -> isotope symbol
				BAI -> symbol
				BAC -> chiral BAH
				BAC -> BAH
				BAC -> chiral
				BAH -> hcount
				symbol -> aliphatic_organic
				symbol -> aromatic_organic
				isotope -> DIGIT
				isotope -> DIGIT DIGIT
				isotope -> DIGIT DIGIT DIGIT
				hcount -> 'H'
				hcount -> 'H' DIGIT
				aliphatic_organic -> 'C'
				aliphatic_organic -> 'F'
				aliphatic_organic -> 'N'
				aliphatic_organic -> 'O'
				aromatic_organic -> 'c'
				aromatic_organic -> 'n'
				aromatic_organic -> 'o'
				DIGIT -> '1'
				DIGIT -> '2'
				DIGIT -> '3'
				DIGIT -> '4'
				DIGIT -> '5'
				chiral -> '@'
				chiral -> '@@'
				bond -> '='
				bond -> '#'
				bond -> '.'
				padding -> '_'
				terminate -> None"""

		self.cfg = nltk.CFG.fromstring(self.grammar_str)
		self.chart_parser = nltk.ChartParser(self.cfg)
		self.prod_rule_n = len(self.cfg.productions())
		self.max_prod_tree_n = max_prod_tree_n  # 125 + 1 = 126
		self.multi_char_tokens = ['Cl', 'Br', '@@']
		self.multi_char_substitute_tokens = ['*', '?', '!']
		self.smiles_type_index = 1

		# Get all production rule labels

		prod_rule_labels = [prod.lhs().symbol() for prod in self.cfg.productions()]

		# Extract unique production rule labels

		self.unique_prod_rule_labels = sorted(set(prod_rule_labels), key = lambda x: prod_rule_labels.index(x))

		# Generate zero mask dummy; M is a (unique_prod_rule_labels x prod_rule_n) Matrix.

		self.mask = np.zeros((len(self.unique_prod_rule_labels), self.prod_rule_n))

		"""
		1. Mask same production rule types in each row
		[1, 0, 0, ...
		[0, 1, 1, 1, 0, ...
		[0, 0, ..., 1]

		2. Save label indices according to its index position
		[0, 1, 1, 1, 2, 2, 2, 2, 3, ..., 20, 21]

		3. Map index to labels
		{'smiles': 0, 'chain': 1, 'branched_atom': 2, ...
		"""

		self.rule_index = []
		self.unique_prod_rule_label_index = {}

		for i, label in enumerate(self.unique_prod_rule_labels):
			label_indices = [k for k, x in enumerate(prod_rule_labels) if x == label]
			self.unique_prod_rule_label_index[label] = i

			for index in label_indices:
				self.mask[i][index] = 1
				self.rule_index.append(i)

		self.rule_index = np.array(self.rule_index)

	def save_data_as_pickle(self, data, name_prefix):
		"""Save Grammar processed data as pickle
		"""

		pickle.dump(data, open(name_prefix + '_{}.pickle'.format(len(data)), "wb"))

	def save_data_as_msgpack(self, data, name_prefix):
		"""Save Grammar processed data as msgpack
		"""

		msgpack.pack(data, open(name_prefix + '.msgpack', "wb"))

	def get_production_rule_n(self, grammar_str):
		"""Get number of production rules
		"""

		return len(nltk.CFG.fromstring(grammar_str).productions())

	def sequences_contain_symbol(self, data, target_symbol):
		"""Sequence contains particular symbol
		"""

		for molecule in data:
			smiles = molecule[3][self.smiles_type_index]

			if type(smiles) is bytes:
				smiles = smiles.decode('UTF-8')

			if target_symbol in smiles:
				print('Symbol {} found'.format(target_symbol))
				return

	def get_tokenized_smiles(self, smiles):
		"""Get tokenized smiles
		"""

		if type(smiles) is bytes:
			smiles = smiles.decode('UTF-8')

		# Substitute multi-symbol tokens

		for i, token in enumerate(self.multi_char_tokens):
			smiles = smiles.replace(token, self.multi_char_substitute_tokens[i])

		# Tokenize smiles

		tokenized_smiles = list(smiles)

		# Back-substitute tokens

		for i, item in enumerate(tokenized_smiles):
			for j, token in enumerate(self.multi_char_substitute_tokens):
				if tokenized_smiles[i] == token:
					tokenized_smiles[i] = self.multi_char_tokens[j]

		return tokenized_smiles

	def get_max_prod_sequence_len(self, data):
		"""Get the lengths of the longes smiles production sequence, SMILES Type 2: 125
		"""

		max_len = 0

		for molecule in data:
			smiles = [molecule[3][self.smiles_type_index]]

			tokenized_smiles = self.get_tokenized_smiles(smiles)

			# Parse smiles production tree, respectively, smiles production sequence

			prod_sequences = self.chart_parser.parse(tokenized_smiles).__next__().productions()

			if len(prod_sequences[0]) > max_len:
				max_len = len(prod_sequences[0])
				print(max_len)

		return max_len

	def smiles_to_grammar_onehot(self, smiles):
		"""Convert smiles to one-hot production rule representation
		"""

		tokenized_smiles = self.get_tokenized_smiles(smiles)

		# Parse smiles production tree, respectively, smiles production sequence

		prod_sequences = self.chart_parser.parse(tokenized_smiles).__next__().productions()

		# Create one hot grammar representation

		one_hot_grammar = [[0 for _ in range(self.prod_rule_n)] for _ in range(self.max_prod_tree_n)]

		for j, prod_rule in enumerate(prod_sequences):
			one_hot_grammar[j][self.cfg.productions().index(prod_rule)] = 1

		return one_hot_grammar

	def get_grammar_representation_training_set(self, data):
		"""Extract one-hot grammar representation for a set of smiles strings
		"""

		one_hot_grammar_data = []

		for mol_i, molecule in enumerate(data):
			smiles = molecule[3][self.smiles_type_index]
			one_hot_grammar_data.append(self.smiles_to_grammar_onehot(smiles))
			print(mol_i)

		return one_hot_grammar_data

	def reconstruct_smiles(self, decoder_output):
		""" Reconstruct unmasked decoder output to smiles string
		"""

		_x = self.sample_production_rules(decoder_output)

		# Convert one-hot production rules to a production sequence representation

		prod_rule_seq = [self.cfg.productions()[_x[i].argmax()] for i in range(self.max_prod_tree_n)]

		# Convert production rule sequence

		sequence = [prod_rule_seq[0].lhs()]

		for prod_rule in prod_rule_seq:

			if str(prod_rule.lhs()) == 'terminate':
				break

			for i, rule in enumerate(sequence):
				if rule == prod_rule.lhs():
					sequence = sequence[:i] + list(prod_rule.rhs()) + sequence[i + 1:]
					break

		# Join to a smiles string, in case all production rules are converted to terminal symbols

		return ''.join(sequence) if all(isinstance(i, str) for i in sequence) else ''

	def sample_production_rules(self, d_out):
		""" Sample productions X from p(X|z), given deterministic decoder output d_out and masks for each production rule
		"""

		_x = np.zeros_like(d_out)

		# Initialize empty stack and push start symbol onto the top

		stack = [self.cfg.productions()[0].lhs().symbol()]

		# Iterate over production sequences

		for i in range(self.max_prod_tree_n):

			# Pop the last-pushed or terminal production rule

			next_rule = self.unique_prod_rule_label_index['terminate']

			if len(stack) > 0:
				next_rule = self.unique_prod_rule_label_index[stack.pop()]

			# Set masked branches of a production rule

			masked_prod_rule = self.mask[next_rule]

			# Sample a particular production rule from the possible branches

			m = masked_prod_rule * np.exp(d_out[i])
			norm = sum(m)
			sampled_output = np.argmax([x / norm for x in m])

			# Fill one-hot matrix with a sampled rule

			_x[i][sampled_output] = 1

			# Get non-terminal production rules on the right-hand side of the relation

			rhs = []

			for r in self.cfg.productions()[sampled_output].rhs():
				if (type(r) == nltk.grammar.Nonterminal) and (str(r) != 'None'):
					rhs.append(str(r))

			# Push production rules in reversed order

			stack.extend(rhs[::-1])

		return _x

