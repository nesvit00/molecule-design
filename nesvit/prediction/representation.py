# Imports

import math
import numpy as np
import nesvit.dataset.qm9 as qm9
from copy import deepcopy


def get_qm9_smiles_training_set(data, label, smiles_type_index):
	"""Convert smiles string to binary matrix
	"""

	i = qm9.QM9_PROPERTY_LABELS.index(label)
	x = []
	y = []
	unique_chars = qm9.SMILES_UNIQUE_CHARS[smiles_type_index]
	max_sample_len = qm9.SMILES_MAX_SAMPLE_LEN[smiles_type_index]

	# Create binary matrix

	for item in data:
		y.append(qm9.transform_property_value_to_0_1_space(item[2][i], label))
		x_smiles = item[3][smiles_type_index].decode('UTF-8')
		x_item = []

		for j in range(max_sample_len):
			xi = [0] * len(unique_chars)

			if j < len(x_smiles):
				ch = x_smiles[j]
				xi[unique_chars.index(ch)] = 1

			x_item.append(xi)

		x.append(x_item)

	return x, y, unique_chars, max_sample_len


def get_qm9_voxelized_charge_training_set(data, label, voxel_n):
	"""Voxelize molecule space. Fill in molecule charge.
	"""

	counter = 0
	total_counter = 0
	i = qm9.QM9_PROPERTY_LABELS.index(label)
	x = []
	y = []

	for molecule in data:
		y.append(qm9.transform_property_value_to_0_1_space(molecule[2][i], label))
		x_item = [[[0 for _ in range(voxel_n)] for _ in range(voxel_n)] for _ in range(voxel_n)]

		# Transform molecule

		mol = translate_molecule_to_center(molecule)
		mol = rotate_molecule_to_principal_axes(mol)

		# Create voxel-space

		for atom in mol[4]:

			atom_label = atom[0].decode('UTF-8')

			# Map positions to [0, 1] space, where 0.5 is center

			pos = qm9.map_pos_to_unit_space_0_1(atom[1])

			# Map atom charge number to voxel space

			voxel_index = list(map(lambda p: math.ceil(voxel_n * p) - 1, pos))

			x_i = voxel_index[0]
			y_i = voxel_index[1]
			z_i = voxel_index[2]
			x_item[x_i][y_i][z_i] = qm9.get_atom_ordinal_number_by_label(atom_label)

		# Print status

		counter += 1
		total_counter += 1

		if counter == 100:
			counter = 0
			print('Voxelized {} of {} samples'.format(total_counter, len(data)))

		x.append(x_item)

	return x, y


def get_qm9_voxelized_training_set(data, label, voxel_n, sigma = 0, omega = 0):
	"""Voxelize molecule space.
	"""

	counter = 0
	total_counter = 0
	i = qm9.QM9_PROPERTY_LABELS.index(label)
	x = []
	y = []

	# for data_i in data_range:
	# 	molecule = data[data_i]

	for molecule in data:
		y.append(qm9.transform_property_value_to_0_1_space(molecule[2][i], label))
		x_item = [[[[0 for _ in range(qm9.ATOM_N)] for _ in range(voxel_n)] for _ in range(voxel_n)] for _ in range(voxel_n)]

		# Transform molecule

		mol = translate_molecule_to_center(molecule)
		mol = rotate_molecule_to_principal_axes(mol)

		# Get number of atoms by type

		atom_num = [0] * qm9.ATOM_N

		for atom in mol[4]:
			atom_label = atom[0].decode('UTF-8')
			atom_num[qm9.get_atom_index_by_label(atom_label)] += 1

		# Create voxel-space

		for atom in mol[4]:
			# Map positions to [0, 1] space, where 0.5 is center

			atom_label = atom[0].decode('UTF-8')
			type_i = qm9.get_atom_index_by_label(atom_label)
			pos = qm9.map_pos_to_unit_space_0_1(atom[1])

			# Map atoms to voxel space

			if sigma == 0:

				# One hot vector

				voxel_index = list(map(lambda p: math.ceil(voxel_n * p) - 1, pos))

				x_i = voxel_index[0]
				y_i = voxel_index[1]
				z_i = voxel_index[2]
				x_item[x_i][y_i][z_i][qm9.get_atom_index_by_label(atom_label)] = 1

			else:

				# Kernel

				voxel_space_pos = list(map(lambda p: voxel_n * p, pos))

				x_i = voxel_space_pos[0]
				y_i = voxel_space_pos[1]
				z_i = voxel_space_pos[2]

				norm = 1 / atom_num[type_i]

				for map_i in range(voxel_n):
					for map_j in range(voxel_n):
						for map_k in range(voxel_n):
							p_sum = (x_i - map_i) ** 2 + (y_i - map_j) ** 2 + (z_i - map_k) ** 2
							h = math.sqrt(p_sum)

							if omega == 0:

								# Gaussian kernel

								kernel = norm * math.exp(-1 * p_sum / sigma)

							else:

								# Wave transform

								wave = math.cos(2 * math.pi * omega * h)
								kernel = wave * norm * math.exp(-1 * p_sum / sigma)

							if h < sigma and kernel > 0:
								x_item[map_i][map_j][map_k][type_i] += kernel

		# Print status

		counter += 1
		total_counter += 1

		if counter == 100:
			counter = 0
			print('Voxelized {} of {} samples'.format(total_counter, len(data)))

		x.append(x_item)

	return x, y


def get_qm9_projected_charge_training_set(data, label, voxel_n, align, augment, evaluation_set = False):
	"""Project data to 3 cube planes. Augment dataset by negating each eigen vector element, respetively.
	"""

	i = qm9.QM9_PROPERTY_LABELS.index(label)
	x = []
	y = []

	proj_n = 3
	ev_n = 1

	if evaluation_set:
		augment = False

	if not align:
		augment = False

	if augment:
		align = True
		ev_n = 9

	for molecule in data:

		# Transform molecule

		mol = molecule
		eig_vectors = None
		ev = None

		if align:
			mol = translate_molecule_to_center(molecule)
			I = get_molecule_inertia_matrix(mol)
			eig_values, eig_vectors = np.linalg.eigh(I)

		for j in range(ev_n):

			if augment:
				# If Eigen vector (i, j) small, skip

				if abs(eig_vectors[math.floor(j / 3)][j % 3]) < 0.1:
					continue

				ev = deepcopy(eig_vectors)
				ev[math.floor(j / 3)][j % 3] *= -1
			else:
				ev = deepcopy(eig_vectors)

			y.append(qm9.transform_property_value_to_0_1_space(molecule[2][i], label))
			x_item = [[[0 for _ in range(proj_n)] for _ in range(voxel_n)] for _ in range(voxel_n)]

			for atom in mol[4]:
				atom_label = atom[0].decode('UTF-8')

				if align:
					pos = np.dot(atom[1], ev).tolist()
				else:
					pos = atom[1]

				pos = qm9.map_pos_to_unit_space_0_1(pos)
				voxel_index = list(map(lambda p: math.ceil(voxel_n * p) - 1, pos))

				x_i = voxel_index[0]
				y_i = voxel_index[1]
				z_i = voxel_index[2]

				atom_ordinal_number = qm9.get_atom_ordinal_number_by_label(atom_label)

				# Projection xy

				x_item[y_i][x_i][0] = atom_ordinal_number

				# Projection xz

				x_item[z_i][x_i][1] = atom_ordinal_number

				# Projection zy

				x_item[y_i][z_i][2] = atom_ordinal_number

			x.append(x_item)

	return x, y


def get_qm9_projected_augmented_onehot_training_set(data, label, voxel_n):
	"""Project data to 3 cube planes. Augment dataset by negating each eigen vector element, respetively.

	COMMENT: Currently the data will not be augmented
	"""

	i = qm9.QM9_PROPERTY_LABELS.index(label)
	x = []
	y = []

	proj_n = 3
	ev_n = 1  # 9

	for molecule in data:

		# Transform molecule

		mol = translate_molecule_to_center(molecule)
		I = get_molecule_inertia_matrix(mol)
		eig_values, eig_vectors = np.linalg.eigh(I)

		for j in range(ev_n):

			# If Eigen vector (i, j) == 0, skip

			# if abs(eig_vectors[math.floor(j / 3)][j % 3]) < 0.0001:
			# 	continue
			#
			# if math.floor(j / 3) == (j % 3):
			# 	continue

			y.append(qm9.transform_property_value_to_0_1_space(molecule[2][i], label))
			x_item = [[[[0 for _ in range(qm9.ATOM_N)] for _ in range(proj_n)] for _ in range(voxel_n)] for _ in range(voxel_n)]

			ev = deepcopy(eig_vectors)
			ev[math.floor(j / 3)][j % 3] *= 1

			for atom in mol[4]:
				atom_label = atom[0].decode('UTF-8')
				pos = np.dot(atom[1], ev).tolist()
				pos = qm9.map_pos_to_unit_space_0_1(pos)
				voxel_index = list(map(lambda p: math.ceil(voxel_n * p) - 1, pos))

				x_i = voxel_index[0]
				y_i = voxel_index[1]
				z_i = voxel_index[2]

				# Projection xy

				x_item[y_i][x_i][0][qm9.ATOM_LABELS.index(atom_label)] = 1

				# Projection xz

				x_item[z_i][x_i][1][qm9.ATOM_LABELS.index(atom_label)] = 1

				# Projection zy

				x_item[y_i][z_i][2][qm9.ATOM_LABELS.index(atom_label)] = 1

			x.append(x_item)

	return x, y


def get_qm9_projected_stacked_onehot_training_set(data, label, voxel_n, proj_n):
	"""Project data to 3 cube planes. Augment dataset by negating each eigen vector element, respetively.
	"""

	i = qm9.QM9_PROPERTY_LABELS.index(label)
	x = []
	y = []

	for molecule in data:

		# Transform molecule

		mol = translate_molecule_to_center(molecule)
		I = get_molecule_inertia_matrix(mol)
		eig_values, eig_vectors = np.linalg.eigh(I)

		y.append(qm9.transform_property_value_to_0_1_space(molecule[2][i], label))
		x_item = [[[0 for _ in range(qm9.ATOM_N)] for _ in range(voxel_n)] for _ in range(proj_n * voxel_n)]

		for atom in mol[4]:
			atom_label = atom[0].decode('UTF-8')
			pos = np.dot(atom[1], eig_vectors).tolist()
			pos = qm9.map_pos_to_unit_space_0_1(pos)
			voxel_index = list(map(lambda p: math.ceil(voxel_n * p) - 1, pos))

			x_i = voxel_index[0]
			y_i = voxel_index[1]
			z_i = voxel_index[2]

			# Projection xy

			x_item[voxel_n * 0 + y_i][x_i][qm9.get_atom_index_by_label(atom_label)] = 1

			# Projection xz

			x_item[voxel_n * 1 + z_i][x_i][qm9.get_atom_index_by_label(atom_label)] = 1

			# Projection zy

			x_item[voxel_n * 2 + y_i][z_i][qm9.get_atom_index_by_label(atom_label)] = 1

		x.append(x_item)

	return x, y


def get_qm9_projected_stacked_charge_training_set(data, label, voxel_n, proj_n):
	"""Project data to 3 cube planes. Augment dataset by negating each eigen vector element, respetively.
	"""

	i = qm9.QM9_PROPERTY_LABELS.index(label)
	x = []
	y = []

	for molecule in data:

		# Transform molecule

		mol = translate_molecule_to_center(molecule)
		mol = rotate_molecule_to_principal_axes(mol)

		y.append(qm9.transform_property_value_to_0_1_space(molecule[2][i], label))
		x_item = [[[0] for _ in range(voxel_n)] for _ in range(proj_n * voxel_n)]

		for atom in mol[4]:
			atom_label = atom[0].decode('UTF-8')
			pos = qm9.map_pos_to_unit_space_0_1(atom[1])
			voxel_index = list(map(lambda p: math.ceil(voxel_n * p) - 1, pos))

			x_i = voxel_index[0]
			y_i = voxel_index[1]
			z_i = voxel_index[2]

			atom_ordinal_number = qm9.get_atom_ordinal_number_by_label(atom_label)

			# Projection xy

			x_item[voxel_n * 0 + y_i][x_i][0] = atom_ordinal_number

			# Projection xz

			x_item[voxel_n * 1 + z_i][x_i][0] = atom_ordinal_number

			# Projection zy

			x_item[voxel_n * 2 + y_i][z_i][0] = atom_ordinal_number

		x.append(x_item)

	return x, y


def translate_molecule_to_center(molecule):
	"""Translate molecule to its center.
	"""

	min_max_x = [100, -100]
	min_max_y = [100, -100]
	min_max_z = [100, -100]

	# Compute min max values of x, y, and z axes

	for i, atom in enumerate(molecule[4]):
		min_max_x[0] = min(min_max_x[0], atom[1][0])
		min_max_y[0] = min(min_max_y[0], atom[1][1])
		min_max_z[0] = min(min_max_z[0], atom[1][2])
		min_max_x[1] = max(min_max_x[1], atom[1][0])
		min_max_y[1] = max(min_max_y[1], atom[1][1])
		min_max_z[1] = max(min_max_z[1], atom[1][2])

	# Align molecule

	c = [min_max_x[0] + (abs(min_max_x[0]) + abs(min_max_x[1])) / 2,
		 min_max_y[0] + (abs(min_max_y[0]) + abs(min_max_y[1])) / 2,
		 min_max_z[0] + (abs(min_max_z[0]) + abs(min_max_z[1])) / 2]

	for i, atom in enumerate(molecule[4]):
		atom[1][0] -= c[0]
		atom[1][1] -= c[1]
		atom[1][2] -= c[2]

	return molecule


def translate_molecule_to_barycenter(molecule):
	"""Translate molecule to its barycenter
	"""

	c = [0, 0, 0]

	# Get mass list

	mass, total_mass = qm9.get_molecule_mass(molecule)

	# Compute center of mass: c

	for i, atom in enumerate(molecule[4]):
		for j, p in enumerate(atom[1]):
			c[j] += mass[i] * p

	c = list(map(lambda x: x / total_mass, c))

	# Translate molecule to the center of mass

	_molecule = deepcopy(molecule)

	for i, atom in enumerate(_molecule[4]):
		for j, p in enumerate(atom[1]):
			atom[1][j] = p - c[j]

	return _molecule


def rotate_molecule_to_principal_axes(molecule):
	"""Rotate molecule to the principal axes origin
	"""

	# Compute inertia tensor matrix: I (symmetric matrix)

	I_xx = I_yy = I_zz = I_xy = I_xz = I_yz = 0.0

	for i, atom in enumerate(molecule[4]):
		p = atom[1]
		I_xx += p[1]**2 + p[2]**2
		I_yy += p[0]**2 + p[2]**2
		I_zz += p[0]**2 + p[1]**2
		I_xy -= p[0] * p[1]
		I_xz -= p[0] * p[2]
		I_yz -= p[1] * p[2]

	I = np.array([
		[I_xx, I_xy, I_xz],
		[I_xy, I_yy, I_yz],
		[I_xz, I_yz, I_zz]])

	# Compute principal axes: eig_vectors

	eig_values, eig_vectors = np.linalg.eigh(I)

	# Rotate molecule

	_molecule = deepcopy(molecule)

	for i, atom in enumerate(_molecule[4]):
		atom[1] = np.dot(atom[1], eig_vectors).tolist()

	return _molecule


def rotate_molecule_to_principal_axes_include_mass(molecule):
	"""Rotate molecule to the principal axes origin
	"""

	# Get molecule mass list

	mass, total_mass = qm9.get_molecule_mass(molecule)

	# Compute inertia tensor matrix: I (symmetric matrix)

	I_xx = I_yy = I_zz = I_xy = I_xz = I_yz = 0.0

	for i, atom in enumerate(molecule[4]):
		p = atom[1]
		I_xx += mass[i] * (p[1]**2 + p[2]**2)
		I_yy += mass[i] * (p[0]**2 + p[2]**2)
		I_zz += mass[i] * (p[0]**2 + p[1]**2)
		I_xy -= mass[i] * p[0] * p[1]
		I_xz -= mass[i] * p[0] * p[2]
		I_yz -= mass[i] * p[1] * p[2]

	I = np.array([
		[I_xx, I_xy, I_xz],
		[I_xy, I_yy, I_yz],
		[I_xz, I_yz, I_zz]])

	# Compute principal axes: eig_vectors

	eig_values, eig_vectors = np.linalg.eigh(I)

	# Rotate molecule

	_molecule = deepcopy(molecule)

	for i, atom in enumerate(_molecule[4]):
		atom[1] = np.dot(atom[1], eig_vectors).tolist()

	return _molecule


def get_molecule_inertia_matrix(molecule):
	"""Rotate molecule to the principal axes origin
	"""

	# Compute inertia tensor matrix: I (symmetric matrix)

	I_xx = I_yy = I_zz = I_xy = I_xz = I_yz = 0.0

	for i, atom in enumerate(molecule[4]):
		p = atom[1]
		I_xx += p[1]**2 + p[2]**2
		I_yy += p[0]**2 + p[2]**2
		I_zz += p[0]**2 + p[1]**2
		I_xy -= p[0] * p[1]
		I_xz -= p[0] * p[2]
		I_yz -= p[1] * p[2]

	return np.array([
		[I_xx, I_xy, I_xz],
		[I_xy, I_yy, I_yz],
		[I_xz, I_yz, I_zz]])

