# Imports

import keras.backend as backend
from keras.models import load_model
from keras.models import Sequential
from keras.models import Model
from keras.layers import *


def get_model(path):
	"""Load a model
	"""
	return load_model(path)


def build_model_1(input_shape, optimizer, loss, metrics):
	"""Build model

			Args:
				input_shape (Tuple[int]): dimension of input sample
				optimizer (optimizer)
				loss (string)
				metrics (List[str])
	"""

	# WORKAROUND: MAE (Percentage)
	# Wrong outcome due to lower K.abs(y_true) term than default (1e-7)

	backend.set_epsilon(1)

	model = Sequential()
	model.add(LSTM(128, input_shape = input_shape, return_sequences = True))
	model.add(LSTM(64))
	model.add(Dropout(0.1))
	model.add(Dense(1))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_1_1(input_shape, optimizer, loss, metrics):
	"""Build model

			Args:
				input_shape (Tuple[int]): dimension of input sample
				optimizer (optimizer)
				loss (string)
				metrics (List[str])
	"""

	# WORKAROUND: MAE (Percentage)
	# Wrong outcome due to lower K.abs(y_true) term than default (1e-7)

	backend.set_epsilon(1)

	model = Sequential()
	model.add(Convolution1D(12, 7, activation = 'relu', input_shape = input_shape))
	model.add(Convolution1D(18, 5, activation = 'relu'))
	model.add(Convolution1D(24, 3, activation = 'relu'))
	model.add(Flatten())
	model.add(BatchNormalization())
	model.add(Dense(256, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(128, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(1))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_2(input_shape, optimizer, loss, metrics):
	"""Build model

			Args:
				input_shape (Tuple[int]): dimension of input sample
				optimizer (optimizer)
				loss (string)
				metrics (List[str])
	"""

	backend.set_epsilon(1)

	model = Sequential()
	model.add(Dense(512, input_shape = input_shape, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(BatchNormalization())
	model.add(Dense(512, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(BatchNormalization())
	model.add(Dense(512, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(BatchNormalization())
	model.add(Dense(256, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_2_1(input_shape, optimizer, loss, metrics):
	"""Build model

			Args:
				input_shape (Tuple[int]): dimension of input sample
				optimizer (optimizer)
				loss (string)
				metrics (List[str])
	"""

	model = Sequential()
	model.add(Dense(512, input_shape = input_shape, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(BatchNormalization())
	model.add(Dense(512, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(BatchNormalization())
	model.add(Dense(512, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(BatchNormalization())
	model.add(Dense(256, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_3(input_shape, optimizer, loss, metrics):
	"""Build model

				Args:
					input_shape (Tuple[int]): dimension of input sample
					optimizer (optimizer)
					loss (string)
					metrics (List[str])
	"""

	backend.set_epsilon(1)

	model = Sequential()
	model.add(Conv2D(8, input_shape = input_shape, kernel_size = (5, 5), activation = 'relu'))
	model.add(MaxPooling2D(pool_size = (2, 2)))
	model.add(Conv2D(12, kernel_size = (3, 3), activation = 'relu'))
	model.add(MaxPooling2D(pool_size = (2, 2)))
	model.add(Flatten())
	model.add(BatchNormalization())
	model.add(Dense(512, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(256, activation = 'relu'))
	model.add(Dense(64, activation = 'relu'))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_4(input_shape, optimizer, loss, metrics):
	"""Build model

				Args:
					input_shape (Tuple[int]): dimension of input sample
					optimizer (optimizer)
					loss (string)
					metrics (List[str])
	"""

	model = Sequential()
	model.add(Conv3D(32, input_shape = input_shape, kernel_size = (5, 5, 5), activation = 'relu', strides = 1))
	model.add(MaxPooling3D(pool_size = (2, 2, 2)))
	model.add(Conv3D(64, kernel_size = (3, 3, 3), activation = 'relu', strides = 1))
	model.add(MaxPooling3D(pool_size = (2, 2, 2)))
	model.add(Flatten())
	model.add(BatchNormalization())
	model.add(Dense(512, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(256, activation = 'relu'))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_5(input_shape, optimizer, loss, metrics):
	"""Build model

				Args:
					input_shape (Tuple[int]): dimension of input sample
					optimizer (optimizer)
					loss (string)
					metrics (List[str])
	"""

	backend.set_epsilon(1)

	model = Sequential()
	model.add(Conv2D(8, input_shape = input_shape, kernel_size = (5, 5), activation = 'relu'))
	model.add(MaxPooling2D(pool_size = (2, 2)))
	model.add(Conv2D(10, kernel_size = (3, 3), activation = 'relu'))
	model.add(MaxPooling2D(pool_size = (2, 2)))
	model.add(Conv2D(12, kernel_size = (2, 2), activation = 'relu'))
	model.add(Flatten())
	model.add(BatchNormalization())
	model.add(Dense(512, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(256, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_5_1(input_shape, optimizer, loss, metrics):
	"""Build model

				Args:
					input_shape (Tuple[int]): dimension of input sample
					optimizer (optimizer)
					loss (string)
					metrics (List[str])
	"""

	backend.set_epsilon(1)

	model = Sequential()
	model.add(Conv3D(8, input_shape = input_shape, kernel_size = (7, 7, 1), activation = 'relu', strides = 1))
	model.add(MaxPooling3D(pool_size = (2, 2, 1)))
	model.add(Conv3D(12, kernel_size = (5, 5, 1), activation = 'relu', strides = 1))
	model.add(MaxPooling3D(pool_size = (2, 2, 1)))
	model.add(Conv3D(18, kernel_size = (3, 3, 1), activation = 'relu', strides = 1))
	model.add(MaxPooling3D(pool_size = (2, 2, 1)))
	model.add(Flatten())
	model.add(BatchNormalization())
	model.add(Dense(512, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(256, activation = 'relu'))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_5_2(input_shape, optimizer, loss, metrics):
	"""Build model

				Args:
					input_shape (Tuple[int]): dimension of input sample
					optimizer (optimizer)
					loss (string)
					metrics (List[str])
	"""

	backend.set_epsilon(1)

	model = Sequential()
	model.add(Conv2D(8, input_shape = input_shape, kernel_size = (5, 5), activation = 'relu', strides = 1))
	model.add(MaxPooling2D(pool_size = (3, 3)))
	model.add(Conv2D(16, input_shape = input_shape, kernel_size = (5, 5), activation = 'relu', strides = 1))
	model.add(MaxPooling2D(pool_size = (2, 2)))
	model.add(Flatten())
	model.add(BatchNormalization())
	model.add(Dense(64, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(64, activation = 'relu'))
	model.add(Dense(64, activation = 'relu'))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_5_2_1(input_shape, optimizer, loss, metrics):
	"""Build model

				Args:
					input_shape (Tuple[int]): dimension of input sample
					optimizer (optimizer)
					loss (string)
					metrics (List[str])
	"""

	backend.set_epsilon(1)

	model = Sequential()
	model.add(Conv2D(8, input_shape = input_shape, kernel_size = (5, 5), activation = 'relu', strides = 1))
	model.add(MaxPooling2D(pool_size = (3, 3)))
	model.add(Conv2D(16, input_shape = input_shape, kernel_size = (5, 5), activation = 'relu', strides = 1))
	model.add(MaxPooling2D(pool_size = (2, 2)))
	model.add(Flatten())
	model.add(BatchNormalization())
	model.add(Dense(256, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(128, activation = 'relu'))
	model.add(Dense(64, activation = 'relu'))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_5_3(input_shape, optimizer, loss, metrics):
	"""Build model

				Args:
					input_shape (Tuple[int]): dimension of input sample
					optimizer (optimizer)
					loss (string)
					metrics (List[str])
	"""

	backend.set_epsilon(1)

	model = Sequential()
	model.add(Conv2D(12, input_shape = input_shape, kernel_size = (5, 5), activation = 'relu', strides = 1))
	model.add(MaxPooling2D(pool_size = (3, 3)))
	model.add(Conv2D(18, input_shape = input_shape, kernel_size = (3, 3), activation = 'relu', strides = 1))
	model.add(MaxPooling2D(pool_size = (2, 2)))
	model.add(Flatten())
	model.add(BatchNormalization())
	model.add(Dense(128, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(64, activation = 'relu'))
	model.add(Dropout(0.1))
	model.add(Dense(64, activation = 'relu'))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_5_4(input_shape, optimizer, loss, metrics):
	"""Build model

				Args:
					input_shape (Tuple[int]): dimension of input sample
					optimizer (optimizer)
					loss (string)
					metrics (List[str])
	"""

	backend.set_epsilon(1)

	model = Sequential()
	model.add(Conv2D(8, input_shape = input_shape, kernel_size = (5, 5), activation = 'relu', strides = 1))
	model.add(MaxPooling2D(pool_size = (3, 3)))
	model.add(Conv2D(16, input_shape = input_shape, kernel_size = (3, 3), activation = 'relu', strides = 1))
	model.add(MaxPooling2D(pool_size = (2, 2)))
	model.add(Flatten())
	model.add(BatchNormalization())
	model.add(Dense(128, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(Dense(64, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(Dense(64, activation = 'relu'))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_5_5(input_shape, optimizer, loss, metrics):
	"""Build model

				Args:
					input_shape (Tuple[int]): dimension of input sample
					optimizer (optimizer)
					loss (string)
					metrics (List[str])
	"""

	backend.set_epsilon(1)

	model = Sequential()
	model.add(Conv2D(8, input_shape = input_shape, kernel_size = (5, 5), activation = 'relu', strides = 1))
	model.add(MaxPooling2D(pool_size = (3, 3)))
	model.add(Conv2D(16, input_shape = input_shape, kernel_size = (3, 3), activation = 'relu', strides = 1))
	model.add(MaxPooling2D(pool_size = (2, 2)))
	model.add(Flatten())
	model.add(BatchNormalization())
	model.add(Dense(256, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(Dense(128, activation = 'relu'))
	model.add(Dropout(0.05))
	model.add(Dense(64, activation = 'relu'))
	model.add(Dense(1, activation = 'sigmoid'))

	model.compile(loss = loss, optimizer = optimizer, metrics = metrics)

	return model


def build_model_comb_smiles_bob_1(input_shape_smiles, input_shape_bob, optimizer, loss, metrics):
	"""Build merged model
	"""

	smiles_input = Input(shape = input_shape_smiles, name = 'smiles_input')

	smiles_branch = LSTM(128, return_sequences = True)(smiles_input)
	smiles_branch = LSTM(64)(smiles_branch)
	smiles_branch = Dropout(0.1)(smiles_branch)

	bob_input = Input(shape = input_shape_bob, name = 'bob_input')
	bob_branch = Dense(512, activation = 'relu')(bob_input)
	bob_branch = Dropout(0.05)(bob_branch)
	bob_branch = Dense(512, activation = 'relu')(bob_branch)
	bob_branch = Dropout(0.05)(bob_branch)
	bob_branch = Dense(512, activation = 'relu')(bob_branch)
	bob_branch = Dropout(0.05)(bob_branch)
	bob_branch = Dense(256, activation = 'relu')(bob_branch)
	bob_branch = Dropout(0.1)(bob_branch)

	# Merge branches

	merged = Concatenate()([smiles_branch, bob_branch])
	merged = BatchNormalization()(merged)

	# Fully connected layers

	merged = Dense(128, activation = 'relu')(merged)
	merged = Dropout(0.05)(merged)
	merged = Dense(64, activation = 'relu')(merged)

	# Output layer

	main_output = Dense(1, name = 'main_output', activation = 'sigmoid')(merged)

	# Compile model

	model = Model(inputs = [smiles_input, bob_input], outputs = main_output)
	model.compile(optimizer = optimizer, loss = loss, metrics = metrics)

	return model


def build_model_comb_smiles_projection_onehot_1(input_shape_smiles, input_shape_proj, optimizer, loss, metrics):
	"""Build merged model
	"""

	backend.set_epsilon(1)

	smiles_input = Input(shape = input_shape_smiles, name = 'smiles_input')

	smiles_branch = LSTM(128, return_sequences = True)(smiles_input)
	smiles_branch = LSTM(64)(smiles_branch)
	smiles_branch = Dropout(0.1)(smiles_branch)

	proj_input = Input(shape = input_shape_proj, name = 'proj_input')
	proj_branch = Conv2D(8, kernel_size = (5, 5), activation = 'relu', strides = 1)(proj_input)
	proj_branch = MaxPooling2D(pool_size = (3, 3))(proj_branch)
	proj_branch = Conv2D(16, kernel_size = (5, 5), activation = 'relu', strides = 1)(proj_branch)
	proj_branch = MaxPooling2D(pool_size = (2, 2))(proj_branch)
	proj_branch = Flatten()(proj_branch)
	proj_branch = BatchNormalization()(proj_branch)
	proj_branch = Dense(256, activation = 'relu')(proj_branch)
	proj_branch = Dropout(0.1)(proj_branch)

	# Merge branches

	merged = Concatenate()([smiles_branch, proj_branch])
	merged = BatchNormalization()(merged)

	# Fully connected layers

	merged = Dense(128, activation = 'relu')(merged)
	merged = Dropout(0.05)(merged)
	merged = Dense(64, activation = 'relu')(merged)

	# Output layer

	main_output = Dense(1, name = 'main_output', activation = 'sigmoid')(merged)

	# Compile model

	model = Model(inputs = [smiles_input, proj_input], outputs = main_output)
	model.compile(optimizer = optimizer, loss = loss, metrics = metrics)

	return model

