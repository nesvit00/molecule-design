# Imports

import random


def print_lable_prediction(label_names, labels, predictions, max_n = None):
	"""Print lables and prodictions
	"""

	print('Labels: {}'.format(label_names))
	print('======')
	print('Label    Prediction')

	# Select some random results

	if max_n is None:
		max_n = len(labels)

	rand_index = random.sample(range(0, len(labels)), max_n)
	rand_index.sort()

	for i in rand_index:
		label = labels[i]
		pred = predictions[i]

		print('{}\t{}'.format(label, pred))


def plot_evaluation(plt, labels, predictions, title):
	"""Plot evaluation
	"""

	plt.rc('xtick', labelsize = 12)
	plt.rc('ytick', labelsize = 12)

	color = []
	for i, item in enumerate(predictions):
		color.append(100000 - abs(predictions[i] - labels[i]) ** 0.3)

	plt.figure(figsize = (5, 5))
	plt.title(title)
	plt.axis([0, 1, 0, 1], fontsize = 12)
	plt.xlim(plt.xlim())
	plt.ylim(plt.ylim())
	plt.plot([-100, 100], [-100, 100], linewidth = 1, c = [110 / 255, 110 / 255, 110 / 255], zorder = 1)
	plt.scatter(labels, predictions, s = 35, marker = '+', c = color, zorder = 2)
	plt.xlabel('True Values', fontsize = 12)
	plt.ylabel('Predictions', fontsize = 12)
	plt.grid(True)
	plt.show(block = True)
	plt.interactive(False)


def plot_training_history(plt, x, x0, title, y_label):
	"""Plot training history
	"""

	plt.plot(x)
	plt.plot(x0)
	plt.title(title)
	plt.ylabel(y_label)
	plt.xlabel('epoch')
	plt.legend(['training', 'validation'], loc = 'upper left')
	plt.show()


def plot_learning_curve(plt, data, title, x_label, y_label):
	"""Plot training history
	"""

	plt.figure(figsize = (5, 5))
	plt.rc('xtick', labelsize = 12)
	plt.rc('ytick', labelsize = 12)

	legend = []
	xticks = [i for i in range(len(data[0][2]))]
	yticks = [2**(i - 1) if i > 0 else 0 for i in range(10)]
	xticklabels = ['1k', '2k', '4k', '8k', '16k', '32k', '64k', '100k'][:len(data[0][2])]
	plt.axes(yticklabels = xticklabels)

	for item in data:
		legend.append(item[1])
		plt.plot(xticks, item[2], item[3])

	plt.title(title)
	plt.xlabel(x_label, fontsize = 12)
	plt.ylabel(y_label, fontsize = 12)
	plt.yscale("symlog")
	plt.yticks(yticks, yticks)
	plt.xticks(xticks, xticklabels)
	plt.rc('text', usetex = True)
	plt.legend(legend, loc = 'lower left', prop = {'size': 14})
	plt.rc('text', usetex = False)
	plt.grid(True)
	plt.show(block = True)
	plt.interactive(False)

