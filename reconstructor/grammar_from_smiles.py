# Imports

import argparse
import msgpack
import nesvit.gvae.parser as gvae_parser
import nesvit.gvae.model as model
import nesvit.prediction.representation as rep

from tensorflow.python.lib.io import file_io


def init(_args):

	# Variables

	max_sample_len = int(_args.max_sample_len)
	latent_dim = int(_args.latent_dim)
	prod_rule_n = int(_args.prod_rule_n)

	# Load data

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))
	x_data, _, unique_chars, max_smiles_len = rep.get_qm9_smiles_training_set(data, 'gap', 1)  # Use "gap" as a dummy input
	rand_index = msgpack.unpack(file_io.FileIO('../input/random_index_30k.msgpack', 'rb'))
	#rand_index = rand_index[:1000]
	rand_index = [i for i in range(1000)]

	print('Data loaded')

	gparser = gvae_parser.GrammarParser(max_sample_len)
	gmodel = model.SMILES2GrammarModel(max_smiles_len, len(unique_chars), max_sample_len, prod_rule_n, latent_dim, gparser.rule_index, gparser.mask, _args.model_path)

	print('Input - Reconstruction - Is identical')
	print('=====================================')
	print('')

	c = 0

	for index in rand_index:

		item = x_data[index]

		# True SMILES string

		smiles = data[index][3][1]

		if type(smiles) is bytes:
			smiles = smiles.decode('UTF-8')

		# Encode to latent space

		z = gmodel.encoder_only.predict([[item]])[0]

		# Decode z

		decoder_output = gmodel.decoder.predict(z)[0]

		# Sample SMILES string from the decoder output

		_smiles = gparser.reconstruct_smiles(decoder_output)

		# Is reconstruction identical

		is_identical = 'False'

		if smiles == _smiles:
			is_identical = 'True'
			c += 1

		# Print result

		print(smiles + ' - ' + _smiles + ' - ' + is_identical)

	# Correct reconstruction rate

	c_rate = c / len(rand_index)

	# Print report

	print('')
	print('Reconstruction rate: ' + str(c_rate))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--input-file',
						default = '../input/data_eval_30000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--model-path',
						default = '',
						required = False)

	parser.add_argument('--max-sample-len',
						default = 126,
						required = False)

	parser.add_argument('--latent-dim',
						default = 36,
						required = False)

	parser.add_argument('--prod-rule-n',
						default = 54,
						required = False)

	args = parser.parse_args()

	init(args)

