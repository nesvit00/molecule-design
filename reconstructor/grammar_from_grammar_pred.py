# Imports

import argparse
import msgpack
import nesvit.gvae.parser as gvae_parser
import nesvit.gvae.model as model
import nesvit.dataset.qm9 as qm9

from tensorflow.python.lib.io import file_io


def init(_args):

	# Load data

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))
	gdata = msgpack.unpack(file_io.FileIO(_args.grammar_input_file, 'rb'))
	rand_index = msgpack.unpack(file_io.FileIO('../input/random_index_30k.msgpack', 'rb'))
	rand_index = rand_index[:1000]

	# Init parser and model

	gparser = gvae_parser.GrammarParser(int(_args.max_sample_len))
	gmodel = model.GrammarModelWithPropertyPrediction2(int(_args.max_sample_len), int(_args.prod_rule_n), int(_args.latent_dim), gparser.rule_index, gparser.mask, _args.model_path)

	print('Input - Reconstruction - Is identical')
	print('=====================================')
	print('')

	c = 0

	for index in rand_index:

		item = gdata[index]

		# True SMILES string

		smiles = data[index][3][1]
		prop = data[index][2][qm9.QM9_PROPERTY_LABELS.index(_args.label)]

		if type(smiles) is bytes:
			smiles = smiles.decode('UTF-8')

		# Encode to latent space

		z = gmodel.encoder.predict([[item]])[0]

		# Predict

		_prop = gmodel.predictor.predict(z)[0][0]
		_prop = qm9.transform_property_value_to_origin_space(_prop, _args.label)

		# Decode z

		decoder_output = gmodel.decoder.predict(z)[0]

		# Sample SMILES string from the decoder output

		_smiles = gparser.reconstruct_smiles(decoder_output)

		# Is reconstruction identical

		is_identical = 'False'

		if smiles == _smiles:
			is_identical = 'True'
			c += 1

		# Print result

		print(smiles + ' - ' + _smiles + ' - ' + is_identical + ' - ' + str(_prop) + ' - ' + str(prop))

	# Correct reconstruction rate

	c_rate = c / len(data)

	# Print report

	print('')
	print('Reconstruction rate: ' + str(c_rate))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--input-file',
						default = '../input/data_eval_30000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--grammar-input-file',
						default = '../input/data_gvae_eval_30000_126.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--model-path',
						default = '',
						required = False)

	parser.add_argument('--label',
						default = 'gap',
						required = False)

	parser.add_argument('--max-sample-len',
						default = 126,
						required = False)

	parser.add_argument('--latent-dim',
						default = 36,
						required = False)

	parser.add_argument('--prod-rule-n',
						default = 54,
						required = False)

	args = parser.parse_args()

	init(args)

