# Imports

import msgpack
import argparse
import nesvit.dataset.qm9 as qm9
from tensorflow.python.lib.io import file_io
import random


def init(_args):

	# Load data

	rand_index = [i for i in range(30000)]
	random.shuffle(rand_index)

	msgpack.pack(rand_index, open('random_index_30k.msgpack', "wb"))

	return

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))

	# Compute RMAE normalizations

	norm = [0 for _ in range(len(qm9.QM9_PROPERTY_LABELS))]
	mean = [0 for _ in range(len(qm9.QM9_PROPERTY_LABELS))]

	for item in data:
		for j, p in enumerate(item):
			mean[j] += p / len(data)

	for item in data:
		for j, p in enumerate(item):
			norm[j] += abs(p - mean[j])

	print(mean)
	print(norm)


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Mandatory Arguments

	parser.add_argument('--input-file',
						default = 'input/data_prop_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	args = parser.parse_args()

	init(args)

