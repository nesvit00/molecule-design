# Imports

import argparse
import msgpack
import qml
from nesvit.dataset import qm9
from tensorflow.python.lib.io import file_io


def init(_args):

	# Variables

	max_atom_n = qm9.MAX_MOLECULE_SIZE  # Computed on QM9
	data_coulomb = []

	# Load data

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))

	for i, item in enumerate(data):
		# Set xyz file path

		file_id = item[0]
		p = [0 for _ in range(6 - len(str(file_id)))]
		num = ''.join(str(x) for x in p)
		xyz_file_path = _args.xyz_data_directory + _args.xyz_file_prefix + num + str(file_id) + '.xyz'

		# Compute Coulomb Matrix

		mol = qml.Compound(xyz = xyz_file_path)
		mol.generate_coulomb_matrix(size = max_atom_n)
		res = mol.representation.tolist()

		# Optimize vector size

		for j, elem in enumerate(res):
			if elem == 0:
				res[j] = 0
			else:
				res[j] = round(elem, 8)

		data_coulomb.append(res)
		print(i)

	msgpack.pack(data_coulomb, open(_args.target_dir + _args.dataset_name + '_{}.msgpack'.format(len(data_coulomb)), "wb"))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--input-file',
						default = 'input/data_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--xyz-data-directory',
						default = '../data/qm9/',
						required = False)

	parser.add_argument('--target-dir',
						default = 'input/',
						help = 'Target directory',
						required = False)

	parser.add_argument('--dataset-name',
						default = 'data_coulomb',
						help = 'Prefix of the dataset file',
						required = False)

	parser.add_argument('--xyz-file-prefix',
						default = 'dsgdb9nsd_',
						required = False)

	args = parser.parse_args()

	init(args)

