# Imports

import msgpack
import argparse
import nesvit.gvae.parser as gvae_parser

from tensorflow.python.lib.io import file_io


def init(_args):

	# Load data

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))

	# Convert data to grammar one-hot representation

	grammer_parser = gvae_parser.GrammarParser(int(_args.max_sample_len))
	one_hot_grammar_data = grammer_parser.get_grammar_representation_training_set(data)

	# Save dataset

	grammer_parser.save_data_as_msgpack(one_hot_grammar_data, 'input/{}_{}_{}'.format(_args.file_name_prefix, len(data), int(_args.max_sample_len)))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--input-file',
						default = 'input/data_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--file-name-prefix',
						default = 'data_gvae',
						required = False)

	parser.add_argument('--max-sample-len',
						default = 126,
						required = False)

	args = parser.parse_args()

	init(args)

