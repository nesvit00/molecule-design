# Imports

import msgpack
import argparse
import nesvit.dataset.qm9 as qm9

from tensorflow.python.lib.io import file_io


def init(_args):

	# Variables

	data_prop = []

	# Load data

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))

	for i, item in enumerate(data):
		prop = item[2]

		for j, _label in enumerate(qm9.QM9_PROPERTY_LABELS):
			prop[j] = qm9.transform_property_value_to_0_1_space(prop[j], _label)

		data_prop.append(prop)

	# Save dataset

	msgpack.pack(data_prop, open(_args.target_dir + _args.file_name_prefix + '_{}.msgpack'.format(len(data_prop)), "wb"))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--input-file',
						default = 'input/data_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--target-dir',
						default = 'input/',
						help = 'Target directory',
						required = False)

	parser.add_argument('--file-name-prefix',
						default = 'data_prop',
						required = False)

	args = parser.parse_args()

	init(args)

