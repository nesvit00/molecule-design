# Imports

import msgpack
import argparse
import nesvit.prediction.representation as rep

from tensorflow.python.lib.io import file_io


def init(_args):

	# Load data

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))

	# Generate voxelized dataset

	t_data, _ = rep.get_qm9_voxelized_charge_training_set(data, 'gap', int(_args.resolution))

	# Save dataset

	msgpack.pack(t_data, open(_args.target_dir + _args.file_name_prefix + '_{}_{}.msgpack'.format(int(_args.resolution), len(t_data)), "wb"))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--input-file',
						default = 'input/data_1000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--target-dir',
						default = 'input/',
						help = 'Target directory',
						required = False)

	parser.add_argument('--file-name-prefix',
						default = 'data_voxel_charge',
						required = False)

	parser.add_argument('--resolution',
						default = 50,
						required = False)

	args = parser.parse_args()

	init(args)

