# Imports

import argparse
from nesvit.dataset import qm9


def init(_args):

	qm9.save_data_as_msgpack(_args.qm9_files_dir, _args.target_dir + _args.dataset_name, int(_args.dataset_size))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--qm9-files-dir',
						default = '../data/qm9',
						help = 'QM9 files (.xyz) directory',
						required = False)

	parser.add_argument('--target-dir',
						default = 'input/',
						help = 'Target directory',
						required = False)

	parser.add_argument('--dataset-name',
						default = 'data',
						help = 'Prefix of the dataset file',
						required = False)

	parser.add_argument('--dataset-size',
						default = 1000,
						help = 'Subset size',
						required = False)

	args = parser.parse_args()

	s = ''
	for a in vars(args):
		s += '--' + a + ' ' + "'" + str(getattr(args, a)) + "'" + ' '
	print(s)

	#init(args)

