# Imports

import msgpack
import argparse

from tensorflow.python.lib.io import file_io


def init(_args):

	# Variables

	smiles = []
	smiles_type_index = 1

	# Load data

	data = msgpack.unpack(file_io.FileIO(_args.input_file, 'rb'))

	for item in data:
		smiles.append(item[3][smiles_type_index].decode('UTF-8'))

	# Save dataset

	msgpack.pack(smiles, open(_args.target_dir + _args.file_name_prefix + '_{}.msgpack'.format(len(smiles)), "wb"))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	# Input Arguments

	parser.add_argument('--input-file',
						default = 'input/data_100000.msgpack',
						help = 'GCS or local path to data file',
						required = False)

	parser.add_argument('--target-dir',
						default = 'input/',
						help = 'Target directory',
						required = False)

	parser.add_argument('--file-name-prefix',
						default = 'data_smiles',
						required = False)

	args = parser.parse_args()

	init(args)

