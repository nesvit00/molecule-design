==================================================
Learning Deep Representations for Molecule Design


Vitali Nesterov

Natural Science Faculty of the University of Basel
Department of Mathematics and Computer Science
Biomedical Data Analysis

9 March, 2019
==================================================




- Download the QM9 dataset from http://quantum-machine.org/datasets/
- Generate a dataset which includes N items from the QM9 in a single file. You can generate more specific data with further "create" scripts using this file as a basis.

python create_qm9_dataset.py --qm9_files_dir '../data/qm9' --target_dir 'input/' --dataset_name 'data' --dataset_size '1000'



1 Regression

1.1 SMILES (LSTM)

python trainer/prop_pred_smiles.py --job_dir '_' --input_file '../input/data_1000.msgpack' --output_dir '../output' --label 'gap' --model_prefix 'm_smiles' --epochs '50' --batch_size '128'



1.2 SMILES (CNN)

python trainer/prop_pred_smiles_conv.py --job_dir '_' --input_file '../input/data_1000.msgpack' --output_dir '../output' --label 'gap' --model_prefix 'm_smiles' --epochs '50' --batch_size '128'



1.3 Voxel (Nuclear Charge)

python trainer/prop_pred_voxel_charge.py --job_dir '_' --input_file '../input/data_1000.msgpack' --output_dir '../output/' --label 'gap' --model_prefix 'm_voxel_charge' --epochs '100' --batch_size '128' --resolution '50'



1.4 Projection (Nuclear Charge)

python trainer/prop_pred_projection_charge_stacked.py --job_dir '_' --input_file '../input/data_1000.msgpack' --output_dir '../output/' --label 'gap' --model_prefix 'm_projection' --epochs '100' --batch_size '128' --resolution '50'



1.5 Projection (One-Hot)

python trainer/prop_pred_projection.py --job_dir '_' --input_file '../input/data_1000.msgpack' --output_dir '../output/' --label 'gap' --model_prefix 'm_projection' --epochs '10' --batch_size '128' --resolution '50'



1.6 Coulomb matrix

python trainer/prop_pred_coulomb.py --job_dir '_' --input_file '../input/data_coulomb_1000.msgpack' --input_prop_file '../input/data_prop_1000.msgpack' --output_dir '../output/' --label 'gap' --model_prefix 'm_coulomb' --epochs '50' --batch_size '128'



1.7 Bag-of-bonds

python trainer/prop_pred_bob.py --job_dir '_' --input_file '../input/data_bob_1000.msgpack' --input_prop_file '../input/data_prop_1000.msgpack' --output_dir '../output/' --label 'gap' --model_prefix 'm_bob' --epochs '50' --batch_size '128'



1.8 Combination of SMILES (LSTM) and Bag-of-Bonds

python trainer/prop_pred_comb_smiles_bob.py --job_dir '_' --input_file '../input/data_1000.msgpack' --input_bob_file '../input/data_bob_1000.msgpack' --output_dir '../output/' --label 'gap' --model_prefix 'm_smiles_bob' --epochs '50' --batch_size '128' 



1.9 Combination of SMILES (LSTM) and Projection (One-Hot)

python trainer/prop_pred_comb_smiles_projection_onehot.py --job_dir '_' --input_file '../input/data_1000.msgpack' --input_bob_file '../input/data_bob_1000.msgpack' --output_dir '../output/' --label 'gap' --model_prefix 'm_smiles_proj_onehot' --resolution '50' --epochs '50' --batch_size '128'




2 Latent representation

2.1 SMILES Grammar to SMILES Grammar

python trainer/latent_grammar_to_grammar.py --job_dir '_' --input_file '../input/data_gvae_1000_126.msgpack' --output_dir '../output/' --model_prefix 'm_gvae' --epochs '100' --batch_size '36' --max_sample_len '126' --latent_dim '36' --prod_rule_n '54' 



2.2 SMILES to SMILES Grammar

python trainer/latent_smiles_to_grammar.py --job_dir '_' --input_file '../input/data_gvae_1000_126.msgpack' --input_smiles_file '../input/data_1000.msgpack' --output_dir '../output/' --model_prefix 'm_smiles_to_gram' --epochs '100' --batch_size '128' --max_sample_len '126' --latent_dim '36' --prod_rule_n '54'



2.3 Bag-of-Bonds to SMILES Grammar

python trainer/latent_bob_to_grammar.py --job_dir '_' --input_file '../input/data_gvae_1000_126.msgpack' --input_bob_file '../input/data_bob_1000.msgpack' --output_dir '../output/' --model_prefix 'm_bob_to_gram' --epochs '100' --batch_size '128' --max_sample_len '126' --latent_dim '36' --prod_rule_n '54' --bob_vector_len '1128'



3 Structuring of latent space

3.1 SMILES Grammar to SMILES Grammar with property prediction

python trainer/latent_grammar_to_grammar_pred.py --job_dir '_' --input_file '../input/data_gvae_1000_126.msgpack' --input_property_file '../input/data_prop_1000.msgpack' --output_dir '../output/' --label 'gap' --model_prefix 'm_gvae_pred' --epochs '100' --batch_size '36' --max_sample_len '126' --latent_dim '36' --prod_rule_n '54'








